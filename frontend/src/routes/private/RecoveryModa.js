import React from "react";
import { useState } from "react";
import { editRecover } from "../../app/slices/sesion/sesionSlice";
import axios from "axios";
import { useDispatch } from "react-redux";
import Swal from "sweetalert2";

import { TextField, Box, Button, Typography, Modal } from "@mui/material";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function RecoveryModa({ open, setOpen, usuario }) {
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const Submit = () => {
    if (password === "") {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Debes ingresar una contraseña!",
      });
      return;
    }
    axios
      .put(`${process.env.REACT_APP_SERVER_SESION_URL}/api/sesion/updatePass`, {
        password: password,
        id_usuario: usuario.id_usuario,
      })
      .then((res) => {
        dispatch(editRecover(1));
        setOpen(false);
        Swal.fire({
          icon: "success",
          title: "Contraseña actualizada",
          text: "Ahora puedes iniciar sesión",
        });
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salio mal!",
        });
      });
  };

  return (
    <Modal
      open={open}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box
        sx={style}
        gap="30px"
        display="grid"
        gridTemplateColumns="repeat(4, minmax(0, 1fr))"
      >
        <Typography
          sx={{ gridColumn: "span 4" }}
          id="modal-modal-title"
          variant="h2"
          component="h2"
        >
            Actualizar contraseña - Obligatorio
        </Typography>
        <TextField
          sx={{ gridColumn: "span 4" }}
          fullWidth
          id="outlined-basic"
          label="password"
          type={"password"}
          variant="outlined"
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          value={password}
        />

        <Box
          sx={{ gridColumn: "span 4" }}
          display="flex"
          justifyContent="flex-end"
        >
          <Button
            variant="contained"
            color="primary"
            sx={{ marginRight: "15px" }}
            onClick={() => {
                Submit();
                setOpen(false);
            }}
          >
            Actualizar
          </Button>
        </Box>
      </Box>
    </Modal>
  );
}
