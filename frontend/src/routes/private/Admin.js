import { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import Sidebar from "../../pages/dashboardAdmin/Sidebar";
import Topbar from "../../pages/dashboardAdmin/Topbar";
import CreateUser from "../../containers/admin/Usuarios/CreateUser";
import ListUsers from "../../containers/admin/Usuarios/ListUsers";
import CreateEstadio from "../../containers/shared/CreateEstadio";
import {
  useGetClientesQuery,
  useGetEmpleadosQuery,
  useGetAdminQuery,
} from "../../app/api/users/apiUsuario";
import { useGetReportesClienteQuery } from "../../app/api/report/reportesAdmin";
import ReportesPremium from "../../containers/admin/Report/ReportPremium";
import { useGetBitacoraQuery } from "../../app/api/report/bitacoraAdmin";
import { useSelector } from "react-redux";

import CreateJugador from "../../containers/shared/CreateJugador";
import ListJugador from "../../containers/shared/ListJugador";
import CreatePartido from "../../containers/shared/CreatePartido";
import ListPartidos from "../../containers/shared/ListPartidos";
import { useGetJugadoresQuery } from "../../app/api/datosDeportivos/apiJugadores";
import { useGetPartidosQuery } from "../../app/api/datosDeportivos/apiPartido";
import CreateEquipo from "../../containers/shared/CreateEquipo";
import CreateArbitro from "../../containers/admin/DatosDeportivos/CreateArbitro";
import ListEstadio from "../../containers/shared/ListEstadio";
import { useGetEquiposQuery } from "../../app/api/datosDeportivos/apiEquipo";
import { useGetEstadiosQuery } from "../../app/api/datosDeportivos/ApiEstadio";
import ListEquipo from "../../containers/shared/ListEquipo";
import { useGetArbitrosQuery } from "../../app/api/datosDeportivos/apiArbitro";
import CreateCompetencia from "../../containers/shared/CreateCompetencia";
// import CreateTecnico from '../../containers/shared/CreateTecnico'

import Reportes from "../../containers/admin/Report/Reportes";
import ListArbitro from "../../containers/admin/DatosDeportivos/ListArbitro";
import ListCompetencia from "../../containers/shared/ListCompetencia";
import { useGetCompetenciasQuery } from "../../app/api/datosDeportivos/apiCompetencia";
import CreateTecnico from "../../containers/shared/CreateTecnico";
import ListTecnico from "../../containers/shared/ListTecnico";
import { useGetTecnicosQuery } from "../../app/api/datosDeportivos/apiTecnico";
import RecoveryModal from "./RecoveryModa";

function AppAdmin() {
  const [isSidebar, setIsSidebar] = useState(true);
  const recover = useSelector((state) => state.sesion.recover);
  const url = useSelector((state) => state.sesion.url);
  const num_grupo = useSelector((state) => state.sesion.num_grupo);

  const { data: clientes, isFetching: fetchClientes } = useGetClientesQuery();
  const { data: empleados, isFetching: fetchEmpleados } =
    useGetEmpleadosQuery();
  const { data: admin, isFetching: fetchAdmins } = useGetAdminQuery();
  const { data: jugadores, isFetching: fetchJugadores } =
    useGetJugadoresQuery();
  const { data: partidos, isFetching: fetchPartidos } = useGetPartidosQuery();
  const { data: equipos, isFetching: fetchEquipos } = useGetEquiposQuery();
  const { data: estadios, isFetching: fetchEstadios } = useGetEstadiosQuery();
  const { data: arbitros, isFetching: fecthArbitros } = useGetArbitrosQuery();
  const { data: competencias, isFetching: fetchCompetencias } =
    useGetCompetenciasQuery();
  const { data: tecnicos, isFetching: fetchTecnicos } = useGetTecnicosQuery();
  const usuario = useSelector((state) => state.sesion.usuario);

  const { data: reportes, isFetching: fetchReportes } =
    useGetReportesClienteQuery(url);

  const { data: bitacora, isFetching: fetchBitacora } = useGetBitacoraQuery({
    id: num_grupo,
    url: url,
  });

  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (recover) {
      setOpen(recover === 2 ? true : false);
    }
  }, [recover]);

  return (
    <div className="app">
      <Sidebar isSidebar={isSidebar} />
      <main className="content">
        <Topbar setIsSidebar={setIsSidebar} />
        <Routes>
          <Route
            path=""
            element={
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "60vh",
                }}
              >
                <h1>Hola Bienvenido</h1>
                <RecoveryModal
                  open={open}
                  setOpen={setOpen}
                  usuario={usuario}
                />
              </div>
            }
          />
          <Route path="addUsuario" element={<CreateUser />} />
          <Route
            path="showClientes"
            element={<ListUsers data={clientes} isFetching={fetchClientes} />}
          />
          <Route
            path="showEmpleados"
            element={<ListUsers data={empleados} isFetching={fetchEmpleados} />}
          />
          <Route
            path="showCompetencias"
            element={
              <ListCompetencia
                data={competencias}
                isFetching={fetchCompetencias}
              />
            }
          />
          <Route
            path="showAdmins"
            element={<ListUsers data={admin} isFetching={fetchAdmins} />}
          />
          <Route path="addJugador" element={<CreateJugador />} />
          <Route
            path="showJugadores"
            element={
              <ListJugador data={jugadores} isFetching={fetchJugadores} />
            }
          />
          <Route path="addPartido" element={<CreatePartido />} />
          <Route
            path="showPartidos"
            element={
              <ListPartidos data={partidos} isFetching={fetchPartidos} />
            }
          />
          <Route
            path="showEstadios"
            element={<ListEstadio data={estadios} isFetching={fetchEstadios} />}
          />
          <Route
            path="showEquipos"
            element={<ListEquipo data={equipos} isFetching={fetchEquipos} />}
          />
          <Route
            path="showArbitros"
            element={<ListArbitro data={arbitros} isFetching={fecthArbitros} />}
          />
          <Route
            path="showTecnicos"
            element={<ListTecnico data={tecnicos} isFetching={fetchTecnicos} />}
          />
          <Route
            path="reporteEmpleados"
            element={
              <Reportes
                title="Reportes Empleados"
                validate={{ uno: true }}
                subtitle="Aqui se muestran los reportes de los empleados con mas/menos noticas publicadas"
              />
            }
          />
          <Route
            path="reporteEmpleados2"
            element={
              <Reportes
                validate={{ uno: true, dos: true }}
                equipos={equipos}
                title="Reportes Empleados"
                subtitle="Aqui se muestran los reportes de los empleados con mas/menos noticas publicadas de X equipo"
              />
            }
          />
          <Route
            path="reporteClientes"
            element={
              <ReportesPremium
                data={reportes?.data}
                isFetching={fetchReportes}
                title="Reportes Clientes"
                subtitle="Aqui se muestran los diferentes reportes de los clientes"
              />
            }
          />
          <Route
            path="reporteClientes2"
            element={
              <Reportes
                title="Reportes Clientes"
                equipos={equipos}
                validate={{ dos: true }}
                subtitle="Aqui se muestran las suscripciones de los clientes a X equipo"
              />
            }
          />
          <Route
            path="reporteAdmin"
            element={
              <Reportes
                datos={bitacora?.data}
                isFetching={fetchBitacora}
                title="Bitacora"
                subtitle="Aqui se muestran las acciones que ha realizado el usuario admin"
              />
            }
          />
          <Route path="addEstadio" element={<CreateEstadio />} />
          <Route path="addEquipo" element={<CreateEquipo />} />
          <Route path="addArbitro" element={<CreateArbitro />} />
          <Route path="addCompetencia" element={<CreateCompetencia />} />
          <Route path="addTecnico" element={<CreateTecnico />} />
          <Route path="*" element={<NotFound showNavbar={false} />} />
        </Routes>
      </main>
    </div>
  );
}

function NotFound() {
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "75vh",
        }}
      >
        <h1>404: Not Found!</h1>
      </div>
    </>
  );
}

export default AppAdmin;
