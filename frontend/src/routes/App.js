import { useEffect } from "react";
import { BrowserRouter, Routes, Route, useLocation } from "react-router-dom";
import Home from "../pages/home/Home";
import LoginPage from "../pages/auth";
import Navbar from "../containers/global/Navbar";
import AppAdmin from "./private/Admin";
import AppCliente from "./private/Cliente";
import AppEmpleado from "./private/Empleado";
import { useSelector } from "react-redux";
import { ProtectedRoute } from "./Protected";
import ConfirmEmail from "../pages/public/ConfirmEmail";
import Esb from "../pages/public/Esb";

const ScrollToTop = () => {
  const { pathname } = useLocation();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);
  return null;
};

function App() {
 
  const isAuth = useSelector((state) => state.sesion.sesion);
  const rol = useSelector((state) => state.sesion.rol);

  return (
    <BrowserRouter>
      <ScrollToTop />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login/:token" element={<LoginPage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/esb" element={<Esb />} />
        <Route element={<ProtectedRoute isAllowed={!!isAuth && rol === 1} />}>
          <Route path="/admin/*" element={<AppAdmin />} />
        </Route>
        <Route element={<ProtectedRoute isAllowed={!!isAuth && rol === 2} />}>
          <Route path="/empleado/*" element={<AppEmpleado />} />
        </Route>
        <Route element={<ProtectedRoute isAllowed={!!isAuth && rol === 3} />}>
          <Route path="/cliente/*" element={<AppCliente />} />
        </Route>
        <Route path="/confirmEmail/:token" element={<ConfirmEmail />} />
        <Route path="/notAllowed" element={<NotAllowed />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

function NotAllowed() {
  return (
    <>
      <Navbar params={false} />
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "80vh",
        }}
      >
        <h1>401: Not Allowed!</h1>
      </div>
    </>
  );
}

function NotFound() {
  return (
    <>
      <Navbar params={true} />
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "75vh",
        }}
      >
        <h1>404: Not Found!</h1>
      </div>
    </>
  );
}

export default App;
