import { configureStore } from "@reduxjs/toolkit";
import { apiUsuario } from "./api/users/apiUsuario";
import { apiEstadio } from "./api/datosDeportivos/ApiEstadio";
import { apiPais } from "./api/func/Pais";
import { apiCliente } from "./api/users/apiCliente";
import { apiNoticias } from "./api/func/Noticias";
import { apiPosicion } from "./api/func/Posicion";
import { apiPartido } from "./api/datosDeportivos/apiPartido";
import { apiEquipo } from "./api/datosDeportivos/apiEquipo";
import { apiJugadores } from "./api/datosDeportivos/apiJugadores";
import { apiArbitro } from "./api/datosDeportivos/apiArbitro";
import { apiCompetencia } from "./api/datosDeportivos/apiCompetencia";
import { apiTransferencias } from "./api/AccionesEmp/apiTransferencias";
import { apiTecnico } from "./api/datosDeportivos/apiTecnico";
import { apiIncidencia } from "./api/AccionesEmp/apiIncidencia";
import { reportesAdminApi } from "./api/report/reportesAdmin";
import { bitacoraAdminApi } from "./api/report/bitacoraAdmin";
import { apiEstadisticas } from "./api/stats/apiEstadisticas";
import { apiSubs } from "./api/stats/apiSubs";
import { apiAsignacion } from "./api/AccionesEmp/apiAsignacion";
import { apiQuiniela } from "./api/stats/apiQuiniela";
import { apiPrediccion } from "./api/Algoritmo/apiPrediccion";

import sesionSlice from "./slices/sesion/sesionSlice";
import predictSlice from "./slices/sesion/predictSlice";
import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  storage,
  version: 1,
  blacklist: [
    "apiUsuario",
    "apiEstadio",
    "apiPais",
    "apiCliente",
    "apiNoticias",
    "apiPosicion",
    "apiPartido",
    "apiEquipo",
    "apiJugadores",
    "apiArbitro",
    "apiCompetencia",
    "apiTransferencias",
    "apiTecnico",
    "apiIncidencia",
    "reportesAdminApi",
    "bitacoraAdminApi",
    "apiEstadisticas",
    "apiSubs",
    "apiAsignacion",
    "apiQuiniela",
    "apiPredriccion",
    "predict"
  ],
};

const persistedReducer = persistReducer(persistConfig, sesionSlice);

export const store = configureStore({
  reducer: {
    sesion: persistedReducer,
    predict: predictSlice,
    [apiUsuario.reducerPath]: apiUsuario.reducer,
    [apiEstadio.reducerPath]: apiEstadio.reducer,
    [apiPais.reducerPath]: apiPais.reducer,
    [apiCliente.reducerPath]: apiCliente.reducer,
    [apiNoticias.reducerPath]: apiNoticias.reducer,
    [apiPosicion.reducerPath]: apiPosicion.reducer,
    [apiPartido.reducerPath]: apiPartido.reducer,
    [apiEquipo.reducerPath]: apiEquipo.reducer,
    [apiJugadores.reducerPath]: apiJugadores.reducer,
    [apiArbitro.reducerPath]: apiArbitro.reducer,
    [apiCompetencia.reducerPath]: apiCompetencia.reducer,
    [apiTransferencias.reducerPath]: apiTransferencias.reducer,
    [apiTecnico.reducerPath]: apiTecnico.reducer,
    [apiIncidencia.reducerPath]: apiIncidencia.reducer,
    [reportesAdminApi.reducerPath]: reportesAdminApi.reducer,
    [bitacoraAdminApi.reducerPath]: bitacoraAdminApi.reducer,
    [apiEstadisticas.reducerPath]: apiEstadisticas.reducer,
    [apiSubs.reducerPath]: apiSubs.reducer,
    [apiAsignacion.reducerPath]: apiAsignacion.reducer,
    [apiQuiniela.reducerPath]: apiQuiniela.reducer,
    [apiPrediccion.reducerPath]: apiPrediccion.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(
      apiUsuario.middleware,
      apiEstadio.middleware,
      apiPais.middleware,
      apiCliente.middleware,
      apiNoticias.middleware,
      apiPosicion.middleware,
      apiPartido.middleware,
      apiEquipo.middleware,
      apiJugadores.middleware,
      apiArbitro.middleware,
      apiCompetencia.middleware,
      apiTransferencias.middleware,
      apiTecnico.middleware,
      apiIncidencia.middleware,
      reportesAdminApi.middleware,
      bitacoraAdminApi.middleware,
      apiEstadisticas.middleware,
      apiSubs.middleware,
      apiAsignacion.middleware,
      apiQuiniela.middleware,
      apiPrediccion.middleware
    ),
});


//----------