import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiTransferencias = createApi({
  reducerPath: "apiTransferencias",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getTransferencias: builder.query({
      query: (obj) => `bitacora/${obj.rol}/${obj.id}`,
      providesTags: ["Transferencias"],
    }),
    addTransferencia: builder.mutation({
      query: (newTransferencia) => ({
        url: `transferir/${newTransferencia.rol}`,
        method: "PUT",
        body: newTransferencia,
      }),
      invalidatesTags: ["Transferencias"],
    }),
  }),
});

export const { useGetTransferenciasQuery, useAddTransferenciaMutation } =
  apiTransferencias;

// Path: frontend\src\app\api\AccionesEmp\apiTransferencias.js
