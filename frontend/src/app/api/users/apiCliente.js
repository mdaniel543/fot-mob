import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";


export const apiCliente = createApi({
  reducerPath: "apiCliente",
  baseQuery: fetchBaseQuery({
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    updateCliente: builder.mutation({
      query: ({updatedCliente, url}) => ({
        url: `${url}/cliente/actualizar/${updatedCliente.id_usuario}`,
        method: "PUT",
        body: updatedCliente,
      }),
    }),
    getMembresia: builder.query({
      query: (id) => `membresia/${id}`,
    }),
    bajaClienteMembresia: builder.mutation({
      query: (updatedCliente) => ({
        url: `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/membresia/baja`,
        method: "PUT",
        body: updatedCliente,
      }),
    }),
    activarClienteMembresia: builder.mutation({
      query: (updatedCliente) => ({
        url: `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/membresia/activar`,
        method: "POST",
        body: updatedCliente,
      }),
    }),
    gethistorialMembresia: builder.query({
      query: ({url, id}) => `${url}/cliente/visualizar/membresias/${id}`,
    }),
    getVisualizarPartidos: builder.query({
      query: (url) => `${url}/cliente/visualizar/partidos`,
    }),
  }),
});

export const {
  useUpdateClienteMutation,
  useGetMembresiaQuery,
  useGethistorialMembresiaQuery,
  useBajaClienteMembresiaMutation,
  useGetVisualizarPartidosQuery,
  useActivarClienteMembresiaMutation,
} = apiCliente;
