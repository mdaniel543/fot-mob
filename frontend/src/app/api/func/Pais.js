import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiPais = createApi({
  reducerPath: "apiPais",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/pais`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getPaises: builder.query({
      query: () => `/`,
      providesTags: ["Paises"],
    }),
  }),
});

export const { useGetPaisesQuery } = apiPais;
