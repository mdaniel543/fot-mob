import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";


export const apiNoticias = createApi({
  reducerPath: "apiNoticias",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/noticias`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getNoticias: builder.query({
      query: () => `/`,
      providesTags: ["Noticias"],
    }),
    createNoticia: builder.mutation({
      query: (body) => ({
        url: "/",
        method: "POST",
        body,
      }),
      invalidatesTags: ["Noticias"],
    }),
  }),
});

export const { useGetNoticiasQuery, useCreateNoticiaMutation } = apiNoticias;
