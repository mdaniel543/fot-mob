import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiQuiniela = createApi({
  reducerPath: "apiQuiniela",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/quiniela`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getQuiniela: builder.query({
      query: (id) => `/${id}`,
      providesTags: ["Quiniela"],
    }),
    postQuiniela: builder.mutation({
      query: (quiniela) => ({
        url: "/",
        method: "POST",
        body: quiniela,
      }),
      invalidatesTags: ["Quiniela"],
    }),
    putQuiniela: builder.mutation({
      query: (quiniela) => ({
        url: "/",
        method: "PUT",
        body: quiniela,
      }),
      invalidatesTags: ["Quiniela"],
    }),
  }),
});

export const {
  useGetQuinielaQuery,
  usePostQuinielaMutation,
  usePutQuinielaMutation,
} = apiQuiniela;
