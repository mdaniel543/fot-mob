import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiEstadisticas = createApi({
  reducerPath: "apiEstadisticas",
  baseQuery: fetchBaseQuery({
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getEstadisticas: builder.query({
      query: (url) => `${url}/cliente/estadistica/jugador`,
    }),
    getEstadisticas2: builder.query({
      query: () => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/jugador2`,
    }),
    getEstadisticas3: builder.query({
      query: ({id, tipo}) => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/jugador3/${id}/${tipo}`,
    }),
    getEstadisticasTecnico: builder.query({
      query: () => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/tecnico`,
    }),
    getEstadisticasEstadio: builder.query({
      query: () => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/estadio`,
    }),
    getEstadisticasPartido: builder.query({
      query: () => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/partido`,
    }),
    getEstadisticasEquipo: builder.query({
      query: () => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/equipo`,
    }),
    getEstadisticasCompetencia: builder.query({
      query: (id) => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/competencia/${id}`,
    }),
    getEstadisticasCompetencia2: builder.query({
      query: (id) => `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/estadistica/competencia2/${id}`,
    }),
  }),
});

export const {
  useGetEstadisticasQuery,
  useGetEstadisticas2Query,
  useGetEstadisticas3Query,
  useGetEstadisticasTecnicoQuery,
  useGetEstadisticasEstadioQuery,
  useGetEstadisticasPartidoQuery,
  useGetEstadisticasEquipoQuery,
  useGetEstadisticasCompetenciaQuery,
  useGetEstadisticasCompetencia2Query,
} = apiEstadisticas;
