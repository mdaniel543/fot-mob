import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiJugadores = createApi({
  reducerPath: "apiJugadores",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/jugador`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getJugadores: builder.query({
      query: () => `/`,
      providesTags: ["Jugadores"],
    }),
    createJugador: builder.mutation({
      query: (newJugador) => ({
        url: "/",
        method: "POST",
        body: newJugador,
      }),
      invalidatesTags: ["Jugadores"],
    }),
    updateJugador: builder.mutation({
      query: (updatedJugador) => ({
        url: `/${updatedJugador.id_jugador}`,
        method: "PUT",
        body: updatedJugador,
      }),
      invalidatesTags: ["Jugadores"],
    }),
    deleteJugador: builder.mutation({
      query: (id) => ({
        url: `/${id}`,
        method: "DELETE",
      }),
      invalidatesTags: ["Jugadores"],
    }),
    getJugadoresByPartido: builder.query({
      query: (idPartido) => `/partido/${idPartido}`,
    })
  }),
});

export const {
  useGetJugadoresQuery,
  useCreateJugadorMutation,
  useUpdateJugadorMutation,
  useDeleteJugadorMutation,
  useGetJugadoresByPartidoQuery,
} = apiJugadores;

// Path: frontend\src\app\api\datosDeportivos\apiJugadores.js
