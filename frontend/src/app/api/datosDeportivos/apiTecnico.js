import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiTecnico = createApi({
  reducerPath: 'apiTecnico',
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/tecnico`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token
      if (token) {
        headers.set('authorization', `Bearer ${token}`)
      }
      return headers
    },
  }),
  endpoints: (builder) => ({
    getTecnicos: builder.query({
      query: () => `/`,
      method: 'GET',
      providesTags: ['Tecnicos'],
    }),
    updateTecnico: builder.mutation({
      query: (updatedTecnico) => ({
        url: `/${updatedTecnico.id_tecnico}`,
        method: 'PUT',
        body: updatedTecnico,
      }),
      invalidatesTags: ['Tecnicos'],
    }),
    createTecnico: builder.mutation({
      query: (newTecnico) => ({
        url: '/',
        method: 'POST',
        body: newTecnico,
      }),
      invalidatesTags: ['Tecnicos'],
    }),
    deleteTecnico: builder.mutation({
      query: (tecnico) => ({
        url: `/${tecnico}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Tecnicos'],
    }),
  }),
})

export const {
  useGetTecnicosQuery,
  useUpdateTecnicoMutation,
  useCreateTecnicoMutation,
  useDeleteTecnicoMutation,
} = apiTecnico
