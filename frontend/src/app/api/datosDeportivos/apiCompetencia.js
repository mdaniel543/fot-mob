import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiCompetencia = createApi({
  reducerPath: 'apiCompetencia',
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/competencia`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token
      if (token) {
        headers.set('authorization', `Bearer ${token}`)
      }
      return headers
    },
  }),
  endpoints: (builder) => ({
    getCompetencias: builder.query({
      query: () => `/`,
      providesTags: ['Competencias'],
    }),
    updateCompetencia: builder.mutation({
      query: (updatedCompetencia) => ({
        url: '/',
        method: 'PUT',
        body: updatedCompetencia,
      }),
      invalidatesTags: ['Competencias'],
    }),
    createCompetencia: builder.mutation({
      query: (newCompetencia) => ({
        url: '/',
        method: 'POST',
        body: newCompetencia,
      }),
      invalidatesTags: ['Competencias'],
    }),
    deleteCompetencia: builder.mutation({
      query: (competencia) => ({
        url: `/${competencia}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Competencias'],
    }),
  }),
})

export const {
  useGetCompetenciasQuery,
  useUpdateCompetenciaMutation,
  useCreateCompetenciaMutation,
  useDeleteCompetenciaMutation,
} = apiCompetencia

// Path: frontend\src\app\api\datosDeportivos\apiDeporte.js
