import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiArbitro = createApi({
  reducerPath: "apiArbitro",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/arbitro`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getArbitros: builder.query({
      query: () => `/`,
      providesTags: ["Arbitros"],
    }),
    updateArbitro: builder.mutation({
      query: (updatedArbitro) => ({
        url: "/",
        method: "PUT",
        body: updatedArbitro,
      }),

      invalidatesTags: ["Arbitros"],
    }),
    createArbitro: builder.mutation({
      query: (newArbitro) => ({
        url: "/",
        method: "POST",
        body: newArbitro,
      }),
      invalidatesTags: ["Arbitros"],
    }),
    deleteArbitro: builder.mutation({
      query: (arbitro) => ({
        url: `/`,
        method: "DELETE",
        body: arbitro,
      }),
      invalidatesTags: ["Arbitros"],
    }),
  }),
});

export const {
  useGetArbitrosQuery,
  useUpdateArbitroMutation,
  useCreateArbitroMutation,
  useDeleteArbitroMutation,
} = apiArbitro;
