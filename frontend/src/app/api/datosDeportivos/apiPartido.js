import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiPartido = createApi({
  reducerPath: "apiPartido",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/partido`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getPartidos: builder.query({
      query: () => `/`,
      providesTags: ["Partidos"],
    }),
    updatePartido: builder.mutation({
      query: (updatedPartido) => ({
        url: "/",
        method: "PUT",
        body: updatedPartido,
      }),

      invalidatesTags: ["Partidos"],
    }),
    createPartido: builder.mutation({
      query: (newPartido) => ({
        url: "/",
        method: "POST",
        body: newPartido,
      }),
      invalidatesTags: ["Partidos"],
    }),
    deletePartido: builder.mutation({
      query: (partido) => ({
        url: `/${partido.id}`,
        method: "DELETE"
      }),
      invalidatesTags: ["Partidos"],
    }),
  }),
});

export const {
  useGetPartidosQuery,
  useUpdatePartidoMutation,
  useCreatePartidoMutation,
  useDeletePartidoMutation,
} = apiPartido;

// Path: frontend\src\app\api\datosDeportivos\apiPartido.js
