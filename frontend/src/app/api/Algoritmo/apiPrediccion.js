import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiPrediccion = createApi({
  reducerPath: "apiPrediccion",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_ALGORITMO_URL}/algoritmo/prediccion`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getPredicciones: builder.query({
        query: (id) => `/${id}`,
        providesTags: ["Predicciones"]
    }),
    crearPrediccion: builder.mutation({
      query: (prediccion) => ({
        url: "/",
        method: "POST",
        body: prediccion,
      }),
      invalidatesTags: ["Predicciones"],
    }),
  }),
});

export const { useCrearPrediccionMutation, useGetPrediccionesQuery } = apiPrediccion;
