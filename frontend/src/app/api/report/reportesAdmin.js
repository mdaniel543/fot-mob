import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const reportesAdminApi = createApi({
  reducerPath: "reportesAdminApi",
  baseQuery: fetchBaseQuery({
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getReportesEmpleado: builder.query({
      query: ({orden, url}) => `${url}/admin/reporte/empleado/${orden}`,
      providesTags: ["ReportesEmpleado"],
    }),
    getReportesEmpleado2: builder.query({
      query: (orden) => `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/reporte/empleado2/${orden.id_orden}/${orden.id_equipo}`,
      providesTags: ["ReportesEmpleado2"],
    }),
    getReportesCliente: builder.query({
      query: (url) => `${url}/admin/reporte/cliente`,
      providesTags: ["ReportesCliente"],
    }),
    getReportesCliente2: builder.query({
      query: (equipo) => `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/reporte/cliente2/${equipo}`,
      providesTags: ["ReportesCliente2"],
    }),
  }),
});

export const {
  useGetReportesEmpleadoQuery,
  useGetReportesEmpleado2Query,
  useGetReportesClienteQuery,
  useGetReportesCliente2Query,
} = reportesAdminApi;
