import Navbar from "../../containers/global/Navbar";
import Swal from "sweetalert2";
import {
  Box,
  Button,
  useMediaQuery,
  Typography,
  useTheme,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import axios from "axios";
import { useDispatch } from "react-redux";
import { editUrl } from "../../app/slices/sesion/sesionSlice";

const initialValuesLogin = {
  grupo: "",
};

const loginSchema = yup.object().shape({
  grupo: yup.string().required("required"),
});

function Esb() {
  const dispatch = useDispatch();
  const { palette } = useTheme();
  const isNonMobile = useMediaQuery("(min-width:600px)");

  const handleFormSubmit = async (values) => {
    const { grupo } = values;
    //dispatch(editUrl("http://service-esb-g8.tk"));
    await axios
      .get(`${process.env.REACT_APP_SERVER_SESION_URL}/api/esb/${grupo}`)
      .then((response) => {
        Swal.fire({
          title: "Se ha seleccionado el servidor correctamente",
          text: "Url: " + response.data.url,
          icon: "success",
          confirmButtonText: "Aceptar",
        });
        dispatch(editUrl({
          url: response.data.url,
          num_grupo: 8
        }));
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonText: "Aceptar",
        });
      });
  };

  return (
    <div className="home">
      <Navbar params={true} />
      <Box>
        <Box
          style={{ marginTop: "3rem" }}
          width="100%"
          backgroundColor={palette.background.alt}
          p="1.5rem 6%"
          textAlign="center"
        >
          <Typography
            fontWeight="bold"
            fontSize="32px"
            color="primary"
          ></Typography>
        </Box>

        <Box
          width={isNonMobile ? "50%" : "93%"}
          p="2rem"
          m="2rem auto"
          borderRadius="1.5rem"
          backgroundColor={palette.background.alt}
        >
          <Typography fontWeight="500" variant="h5" sx={{ mb: "1.5rem" }}>
            Seleccione el servidor a consultar
          </Typography>
          <Formik
            onSubmit={handleFormSubmit}
            initialValues={initialValuesLogin}
            validationSchema={loginSchema}
          >
            {({ values, handleChange, handleSubmit, resetForm }) => (
              <form onSubmit={handleSubmit}>
                <Box
                  display="grid"
                  gap="30px"
                  gridTemplateColumns="repeat(4, minmax(0, 1fr))"
                  sx={{
                    "& > div": {
                      gridColumn: isNonMobile ? undefined : "span 4",
                    },
                  }}
                >
                  <FormControl
                    fullWidth
                    sx={{
                      gridColumn: "span 4",
                    }}
                  >
                    <InputLabel id="demo-simple-select-label">
                      Grupos
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.grupo}
                      name="grupo"
                      label="Grupos"
                      onChange={handleChange}
                    >
                      <MenuItem value={1}>Grupo 1</MenuItem>
                      <MenuItem value={2}>Grupo 2</MenuItem>
                      <MenuItem value={3}>Grupo 3</MenuItem>
                      <MenuItem value={4}>Grupo 4</MenuItem>
                      <MenuItem value={5}>Grupo 5</MenuItem>
                      <MenuItem value={6}>Grupo 6</MenuItem>
                      <MenuItem value={7}>Grupo 7</MenuItem>
                      <MenuItem value={8}>Grupo 8</MenuItem>
                      <MenuItem value={9}>Grupo 9</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
                {/* BUTTONS */}
                <Box>
                  <Button
                    fullWidth
                    type="submit"
                    sx={{
                      m: "1.5rem 0",
                      p: "1rem",
                      backgroundColor: palette.primary.main,
                      color: palette.background.alt,
                      "&:hover": { color: palette.primary.main },
                    }}
                  >
                    Solicitar Servicio
                  </Button>
                </Box>
              </form>
            )}
          </Formik>
        </Box>
      </Box>
    </div>
  );
}

export default Esb;
