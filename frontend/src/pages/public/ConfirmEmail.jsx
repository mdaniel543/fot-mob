import React from "react";
import Navbar from "../../containers/global/Navbar";
import { isExpired, decodeToken } from "react-jwt";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
import { Box, Button, Typography } from "@mui/material";

function ConfirmEmail() {
  const { token } = useParams();
  const decodedToken = decodeToken(token);
  const isTokenExpired = isExpired(token);

  const handleSumbit = async () => {
    try {
      await axios.post(
        `${process.env.REACT_APP_SERVER_SESION_URL}/api/sesion/confirmar`,
        { token }
      );
      Swal.fire({
        title: "¡Correo confirmado!",
        text: "Ahora puedes iniciar sesión en la plataforma",
        icon: "success",
        confirmButtonText: "Aceptar",
      });
    } catch (error) {
      Swal.fire({
        title: "¡Error!",
        text: "No se pudo confirmar el correo electrónico",
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
  };

  return (
    <>
      <Navbar />
      <Box display="grid" bgcolor={"#F0F0F0"} height="100vh">
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {isTokenExpired ? (
            <h1>El token ha expirado</h1>
          ) : (
            <Typography fontWeight="500" variant="h3" sx={{ mt: "2rem" }}>
              Confirma tu correo electrónico: 
              {decodedToken?.correo} 
              para poder iniciar sesión en la plataforma
            </Typography>
          )}
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Button
            variant="contained"
            size="large"
            color="primary"
            disabled={isTokenExpired}
            onClick={handleSumbit}
          >
            Confirmar correo
          </Button>
        </div>
      </Box>
    </>
  );
}

export default ConfirmEmail;
