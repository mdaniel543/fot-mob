import { Box, IconButton } from "@mui/material";
import NotificationsOutlinedIcon from "@mui/icons-material/NotificationsOutlined";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import logo from "../../assets/Logo_con_slogan.png";
import CardMedia from "@mui/material/CardMedia";
import LogoutIcon from "@mui/icons-material/Logout";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { cerrarSesion } from "../../app/slices/sesion/sesionSlice";

const Topbar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  return (
    <Box display="flex" justifyContent="space-between" p={2}>
      <Box display="flex" borderRadius="3px" marginTop="-50px">
        {/*logo */}
        <CardMedia
          image={logo}
          component="img"
          alt={`logo`}
          height="150"
          width="150"
          sx={{ borderRadius: "3px" }}
        />
      </Box>

      {/* ICONS */}
      <Box display="flex" marginTop="-40px">
        <IconButton
          sx={{
            "&:hover": {
              outline: "none",
              backgroundColor: "transparent",
            },
          }}
        >
          <NotificationsOutlinedIcon />
        </IconButton>
        <IconButton
          sx={{
            "&:hover": {
              outline: "none",
              backgroundColor: "transparent",
            },
          }}
        >
          <PersonOutlinedIcon />
        </IconButton>
        <IconButton
          onClick={() => {
            dispatch(cerrarSesion());
            navigate("/");
          }}
          sx={{
            "&:hover": {
              outline: "none",
              backgroundColor: "transparent",
            },
          }}
        >
          <LogoutIcon />
        </IconButton>
      </Box>
    </Box>
  );
};

export default Topbar;
