import MainCarousel from "./MainCarousel";
import ListInfo from "./ListInfo";
import Footer from "../../containers/global/Footer";
import Navbar from "../../containers/global/Navbar";

function Home() {
  return (
    <div className="home">
      <Navbar params={true}/>
      <MainCarousel />
      <ListInfo />
      <Footer />
    </div>
  );
}

export default Home;