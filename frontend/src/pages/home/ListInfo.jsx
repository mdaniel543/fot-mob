import React, { useState } from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";

const ListInfo = () => {
  const [value, setValue] = useState("Mision");
  const breakPoint = useMediaQuery("(min-width:600px)");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box width="80%" margin="80px auto">
      <Typography variant="h3" textAlign="center">
        Nuestra <b>Empresaaaaaaaa</b>
      </Typography>
      <Tabs
        textColor="primary"
        indicatorColor="primary"
        value={value}
        onChange={handleChange}
        centered
        TabIndicatorProps={{ sx: { display: breakPoint ? "block" : "none" } }}
        sx={{
          m: "25px",
          "& .MuiTabs-flexContainer": {
            flexWrap: "wrap",
          },
        }}
      >
        <Tab label="Mision" value="Mision" />
        <Tab label="Vision" value="Vision" />
      </Tabs>
      <Box
        margin="0 auto"
        justifyContent="space-around"
        rowGap="20px"
        columnGap="1.33%"
      >
        {value === "Mision" ? (
          <Box>
            <Typography variant="h4" textAlign="center">
              Nuestra <b>Mision</b>
            </Typography>
            <Typography
              variant="body1"
              textAlign="center"
              justifyContent="center"
              sx={{ marginTop: "15px" }}
            >
              El futbol es un deporte muy importante para una gran mayoría de
              personas, por lo que sabemos que hay que presentar una alternativa
              confiable y agradable al usuario. Un método en el que el resultado
              final también es bueno para cada tipo de cliente que utiliza
              nuestra plataforma. Por eso creamos una plataforma que combina la
              buena experiencia de uso con los mejores algoritmos de predicción,
              y nuestra misión es hacer vivir el deporte en nuestros clientes de
              una mejor manera fuera de lo convencional.
            </Typography>
          </Box>
        ) : (
          <Box>
            <Typography variant="h4" textAlign="center">
              Nuestra <b>Vision</b>
            </Typography>
            <Typography
              variant="body1"
              textAlign="center"
              justifyContent="center"
              sx={{ marginTop: "15px" }}
            >
              Pateo bonito se fundó con la idea de que con un plan de trabajo
              constante se puede alcanzar un crecimiento sostenible y de
              calidad. Queremos marcar tendencias y pertenecer a la nueva era
              del futbol. Queremos mantener en nuestra aplicación un inicio
              personalizado para que nuestros usuarios puedan estar al tanto de
              la ultima noticia de sus equipos favoritos, obtengan contenido
              original y en un futuro tener diferentes opciones para que los
              usuarios puedan vivir mas cerca que nunca el color de sus
              equipos favoritos.
            </Typography>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default ListInfo;
