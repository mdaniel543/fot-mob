import { useState } from "react";
import { ProSidebar, Menu, MenuItem } from "react-pro-sidebar";
import { Box, IconButton, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import "react-pro-sidebar/dist/css/styles.css";
import { shades } from "../../style/theme";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import AccountBoxOutlinedIcon from "@mui/icons-material/AccountBoxOutlined";
import AccountBalanceWalletOutlinedIcon from "@mui/icons-material/AccountBalanceWalletOutlined";
import WorkHistoryOutlinedIcon from "@mui/icons-material/WorkHistoryOutlined";
import { useSelector } from "react-redux";
import SportsOutlinedIcon from "@mui/icons-material/SportsOutlined";
import AssessmentRoundedIcon from '@mui/icons-material/AssessmentRounded';
import SportsEsportsOutlinedIcon from '@mui/icons-material/SportsEsportsOutlined';
import GrainIcon from '@mui/icons-material/Grain';
import useMediaQuery from "@mui/material/useMediaQuery";
import ShowChartOutlinedIcon from '@mui/icons-material/ShowChartOutlined';

const Item = ({ title, to, icon, selected, setSelected }) => {
  return (
    <MenuItem
      active={selected === title}
      style={{
        color: shades.grey[100],
      }}
      onClick={() => setSelected(title)}
      icon={icon}
    >
      <Typography>{title}</Typography>
      <Link to={"/cliente" + to} />
    </MenuItem>
  );
};

const Sidebar = ({ membresia }) => {
  const usuario = useSelector((state) => state.sesion.usuario);
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isCollapsed, setIsCollapsed] = useState(!isNonMobile)
  const [selected, setSelected] = useState("Dashboard");
  return (
    <Box
      sx={{
        "& .pro-sidebar-inner": {
          background: `${shades.blueAccent[400]} !important`,
        },
        "& .pro-icon-wrapper": {
          backgroundColor: "transparent !important",
        },
        "& .pro-inner-item": {
          padding: "5px 35px 5px 20px !important",
        },
        "& .pro-inner-item:hover": {
          color: "#DCBBA1 !important",
        },
        "& .pro-menu-item.active": {
          color: "#B78454 !important",
        },
      }}
    >
      <ProSidebar collapsed={isCollapsed}>
        <Menu iconShape="square">
          {/* LOGO AND MENU ICON */}
          <MenuItem
            onClick={() => setIsCollapsed(!isCollapsed)}
            icon={isCollapsed ? <MenuOutlinedIcon /> : undefined}
            style={{
              margin: "10px 0 20px 0",
              color: shades.grey[100],
            }}
          >
            {!isCollapsed && (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                ml="10px"
              >
                <Typography variant="h5" color={shades.grey[100]}>
                  CLIENTE
                </Typography>
                <IconButton onClick={() => setIsCollapsed(!isCollapsed)}>
                  <MenuOutlinedIcon />
                </IconButton>
              </Box>
            )}
          </MenuItem>

          {!isCollapsed && (
            <Box mb="25px">
              <Box display="flex" justifyContent="center" alignItems="center">
                <img
                  alt="profile-user"
                  width="120px"
                  height="120px"
                  src={usuario.fotografia}
                  style={{ cursor: "pointer", borderRadius: "50%" }}
                />
              </Box>
              <Box textAlign="center">
                <Typography
                  variant="h3"
                  color={shades.grey[100]}
                  fontWeight="bold"
                  sx={{ m: "10px 0 0 0" }}
                >
                  {usuario.nombre} {usuario.apellido}
                </Typography>
                <Typography variant="h5" color={shades.primary[700]}>
                  {usuario.correo}
                </Typography>
              </Box>
            </Box>
          )}

          <Box paddingLeft={isCollapsed ? undefined : "8%"}>
            <Item
              title="Home"
              to="/"
              icon={<HomeOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Typography
              variant="h6"
              color={shades.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Perfil
            </Typography>
            <Item
              title="Editar Perfil"
              to="/editCliente"
              icon={<AccountBoxOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Typography
              variant="h6"
              color={shades.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Partidos
            </Typography>
            <Item
              title="Ver Partidos"
              to="/partidos"
              icon={<SportsOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Typography
              variant="h6"
              color={shades.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Membresia
            </Typography>
            <Item
              title="Estado Membresia"
              to="/membresia"
              icon={<AccountBalanceWalletOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Historial de Membresias"
              to="/historialMembresias"
              icon={<WorkHistoryOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            {membresia === 2 && (
              <>
               <Typography
                  variant="h6"
                  color={shades.grey[300]}
                  sx={{ m: "15px 0 5px 20px" }}
                >
                  Premium
                </Typography>
                <Item
                  title="Suscribirse" 
                  to="/suscribirse"
                  icon={<AccountBalanceWalletOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Ver Noticias"
                  to="/noticias"
                  icon={<SportsEsportsOutlinedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Quiniela"
                  to="/quinielas"
                  icon={<GrainIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Ver Predicciones"
                  to="/predicciones"
                  icon={<ShowChartOutlinedIcon/>}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Typography
                  variant="h6"
                  color={shades.grey[300]}
                  sx={{ m: "15px 0 5px 20px" }}
                >
                  Reportes Estadisticos
                </Typography>
                <Item
                  title="Reporte Jugadores 1"
                  to="/reporteJugadores1"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Jugadores 2"
                  to="/reporteJugadores2"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Jugadores E"
                  to="/reporteJugadores3"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Tecnico E"
                  to="/reporteTecnicoE"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Tecnico"
                  to="/reporteTecnico"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Partidos"
                  to="/reportePartidos"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Estadios"  
                  to="/reporteEstadios"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Equipos"
                  to="/reporteEquipos"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Competencias1"
                  to="/reporteCompetencias1"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
                <Item
                  title="Reporte Competencias2"
                  to="/reporteCompetencias2"
                  icon={<AssessmentRoundedIcon />}
                  selected={selected}
                  setSelected={setSelected}
                />
              </>
            )}
          </Box>
        </Menu>
      </ProSidebar>
    </Box>
  );
};

export default Sidebar;
