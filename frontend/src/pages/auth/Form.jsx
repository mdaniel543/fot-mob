import React from "react";
import { useState } from "react";
import {
  Box,
  Button,
  TextField,
  useMediaQuery,
  Typography,
  useTheme,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { Formik } from "formik";
import * as yup from "yup";
import Dropzone from "react-dropzone";
import FlexBetween from "../../components/Flex";
import InputAdornment from "@mui/material/InputAdornment";
import TodayIcon from "@mui/icons-material/Today";
import { useNavigate } from "react-router-dom";
import { useGetPaisesQuery } from "../../app/api/func/Pais";
import axios from "axios";
import Swal from "sweetalert2";
import { useDispatch } from "react-redux";
import { iniciarSesion } from "../../app/slices/sesion/sesionSlice";
import { isExpired, decodeToken } from "react-jwt";

import { useSelector } from "react-redux";

const phoneRegExp =
  /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?[0-9]{3,4}[ -]?[0-9]{3,4}$/;

const registerSchema = yup.object().shape({
  nombre: yup.string().required("Campo requerido"),
  apellido: yup.string().required("Campo requerido"),
  correo: yup.string().email("correo invalido").required("Campo requerido"),
  direccion: yup.string().required("Campo requerido"),
  telefono: yup
    .string()
    .matches(phoneRegExp, "Numero invalido")
    .required("Campo requerido"),
  password: yup.string().required("Campo requerido"),
  id_genero: yup.string().required("Campo requerido"),
  fecha_nac: yup.string().required("Campo requerido"),
  id_pais: yup.string().required("Campo requerido"),
  fotografia: yup.mixed().required("Campo requerido"),
});

const loginSchema = yup.object().shape({
  correo: yup.string().email("invalid email").required("required"),
  password: yup.string().required("required"),
});

const initialValuesRegister = {
  apellido: "",
  nombre: "",
  correo: "",
  direccion: "",
  telefono: "",
  edad: "", // no se usa
  password: "",
  id_genero: "",
  fecha_nac: "",
  id_pais: "",
  id_rol: "3",
  picture: "",
  fotografia: "",
  estado: "Alta",
};

const initialValuesLogin = {
  correo: "",
  password: "",
};

const Form = () => {
  const url = useSelector((state) => state.sesion.url);
  const num_grupo = useSelector((state) => state.sesion.num_grupo);
  const [pageType, setPageType] = useState("login");
  const { palette } = useTheme();
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const isLogin = pageType === "login";
  const isRegister = pageType === "register";
  const navigate = useNavigate();
  const { data: paises = [] } = useGetPaisesQuery();

  const register = async (values, onSubmitProps) => {
    let data = {
      ...values,
      clave_acceso: values.password,
      id_pais: parseInt(values.id_pais),
      id_genero: parseInt(values.id_genero),
      id_rol: 3,
      picture: "",
    };
    console.log(data);
    try {
      await axios.post(`${url}/api/sesion/crear`, data);
      Swal.fire({
        title: "Usuario registrado",
        text: "El usuario se registró correctamente",
        icon: "success",
        confirmButtonText: "Aceptar",
      });
    } catch (error) {
      Swal.fire({
        title: "Error",
        text: error.response.data.message,
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
    setPageType(isLogin ? "register" : "login");
    //onSubmitProps.resetForm();
  };

  const dispatch = useDispatch();

  const login = async (values, onSubmitProps) => {
    console.log(values);
    try {
      const { data } = await axios.post(`${url}/api/sesion/iniciar`, {
        ...values,
        num_grupo: 8,
      });
      console.log(data);
      const decodedToken = decodeToken(data.token);
      console.log(decodedToken);
      if (isExpired(data.token)) {
        Swal.fire({
          title: "Error",
          text: "El token ha expirado",
          icon: "error",
          confirmButtonText: "Aceptar",
        });
        return;
      }
      dispatch(
        iniciarSesion({
          token: data.token,
          usuario: decodedToken,
          rol: decodedToken.id_rol,
          recover: data.tipo,
        })
      );
      if (decodedToken.id_rol === 1) navigate("/admin");
      if (decodedToken.id_rol === 2) navigate("/empleado");
      if (decodedToken.id_rol === 3) navigate("/cliente");
    } catch (error) {
      Swal.fire({
        title: "Error",
        text: error.response.data.message,
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
    onSubmitProps.resetForm();
  };

  const recoverPassword = async () => {
    console.log("recuperar contraseña");
    Swal.fire({
      title: "Recuperar contraseña",
      text: "Ingrese su correo para recuperar su contraseña",
      input: "email",
      inputPlaceholder: "Ingrese su correo",
      showCancelButton: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          await axios.post(
            `${process.env.REACT_APP_SERVER_SESION_URL}/api/sesion/recuperar`,
            {
              correo: result.value,
            }
          );
          Swal.fire({
            title: "Correo enviado",
            text: "Se ha enviado un correo para recuperar su contraseña",
            icon: "success",
            confirmButtonText: "Aceptar",
          });
        } catch (error) {
          Swal.fire({
            title: "Error",
            text: "Ocurrió un error al enviar el correo",
            icon: "error",
            confirmButtonText: "Aceptar",
          });
        }
      }
    });
  };

  const handleFormSubmit = async (values, onSubmitProps) => {
    if (isLogin) await login(values, onSubmitProps);
    if (isRegister) await register(values, onSubmitProps);
  };

  return (
    <Formik
      onSubmit={handleFormSubmit}
      initialValues={isLogin ? initialValuesLogin : initialValuesRegister}
      validationSchema={isLogin ? loginSchema : registerSchema}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        resetForm,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            gap="30px"
            gridTemplateColumns="repeat(4, minmax(0, 1fr))"
            sx={{
              "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
            }}
          >
            {isRegister && (
              <>
                <TextField
                  label="Nombres"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.nombre}
                  name="nombre"
                  error={Boolean(touched.nombre) && Boolean(errors.nombre)}
                  helperText={touched.nombre && errors.nombre}
                  sx={{ gridColumn: "span 2" }}
                />
                <TextField
                  label="Apellidos"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.apellido}
                  name="apellido"
                  error={Boolean(touched.apellido) && Boolean(errors.apellido)}
                  helperText={touched.apellido && errors.apellido}
                  sx={{ gridColumn: "span 2" }}
                />
                <TextField
                  label="Direccion"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.direccion}
                  name="direccion"
                  error={
                    Boolean(touched.direccion) && Boolean(errors.direccion)
                  }
                  helperText={touched.direccion && errors.direccion}
                  sx={{ gridColumn: "span 4" }}
                />
                <TextField
                  label="Telefono"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.telefono}
                  name="telefono"
                  error={Boolean(touched.telefono) && Boolean(errors.telefono)}
                  helperText={touched.telefono && errors.telefono}
                  sx={{ gridColumn: "span 4" }}
                />
                <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                  <InputLabel id="demo-simple-select-label">Genero</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    label="Genero"
                    id="demo-simple-select"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.id_genero}
                    name="id_genero"
                    error={
                      Boolean(touched.id_genero) && Boolean(errors.id_genero)
                    }
                    helperText={touched.id_genero && errors.id_genero}
                  >
                    <MenuItem value="2">Masculino</MenuItem>
                    <MenuItem value="1">Femenino</MenuItem>
                  </Select>
                </FormControl>
                <TextField
                  label="Fecha de nacimiento"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.fecha_nac}
                  name="fecha_nac"
                  format="dd-MM-yyyy"
                  error={
                    Boolean(touched.fecha_nac) && Boolean(errors.fecha_nac)
                  }
                  helperText={touched.fecha_nac && errors.fecha_nac}
                  sx={{ gridColumn: "span 4" }}
                  type="date"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <TodayIcon />
                      </InputAdornment>
                    ),
                  }}
                />
                <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                  <InputLabel id="demo-simple-select-label-1">Pais</InputLabel>
                  <Select
                    labelId="demo-simple-select-label-1"
                    label="Pais"
                    id="demo-simple-select-1"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.id_pais}
                    name="id_pais"
                    error={Boolean(touched.id_pais) && Boolean(errors.id_pais)}
                    helperText={touched.id_pais && errors.id_pais}
                  >
                    {paises.map((pais) => (
                      <MenuItem value={pais.id_pais}>{pais.nombre}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <Box
                  gridColumn="span 4"
                  border={`1px solid ${palette.neutral.medium}`}
                  borderRadius="5px"
                  p="1rem"
                >
                  <Dropzone
                    acceptedFiles=".jpg,.jpeg,.png"
                    multiple={false}
                    onDrop={(acceptedFiles) =>
                      new Promise((resolve, reject) => {
                        const fileReader = new FileReader();
                        fileReader.readAsDataURL(acceptedFiles[0]);
                        fileReader.onload = () => {
                          setFieldValue(
                            "picture",
                            acceptedFiles[0],
                            setFieldValue(
                              "fotografia",
                              fileReader.result.split(",")[1]
                            )
                          );
                        };
                        fileReader.onerror = (error) => {
                          reject(error);
                        };
                      })
                    }
                  >
                    {({ getRootProps, getInputProps }) => (
                      <Box
                        {...getRootProps()}
                        border={`2px dashed ${palette.primary.main}`}
                        p="1rem"
                        sx={{ "&:hover": { cursor: "pointer" } }}
                      >
                        <input {...getInputProps()} />
                        {!values.picture ? (
                          <p>Add Picture Here</p>
                        ) : (
                          <FlexBetween>
                            <Typography>{values.picture.name}</Typography>
                            <EditOutlinedIcon />
                          </FlexBetween>
                        )}
                      </Box>
                    )}
                  </Dropzone>
                </Box>
              </>
            )}

            <TextField
              label="Email"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.correo}
              name="correo"
              error={Boolean(touched.correo) && Boolean(errors.correo)}
              helperText={touched.correo && errors.correo}
              sx={{ gridColumn: "span 4" }}
            />
            <TextField
              label="Password"
              type="password"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.password}
              name="password"
              error={Boolean(touched.password) && Boolean(errors.password)}
              helperText={touched.password && errors.password}
              sx={{ gridColumn: "span 4" }}
            />
          </Box>
          {/* BUTTONS */}
          <Box>
            <Typography
              sx={{
                textDecoration: "underline",
                color: palette.primary.main,
                marginTop: "1.2rem",
                "&:hover": {
                  cursor: "pointer",
                  color: palette.primary.light,
                },
                textAlign: "right",
              }}
              onClick={recoverPassword}
            >
              {isLogin ? "¿Olvidaste tu contraseña?" : ""}
            </Typography>

            <Button
              fullWidth
              type="submit"
              sx={{
                m: "1.5rem 0",
                p: "1rem",
                backgroundColor: palette.primary.main,
                color: palette.background.alt,
                "&:hover": { color: palette.primary.main },
              }}
            >
              {isLogin ? "INCIAR SESION" : "REGISTRARME"}
            </Button>

            <Typography
              onClick={() => {
                setPageType(isLogin ? "register" : "login");
                resetForm();
              }}
              sx={{
                textDecoration: "underline",
                color: palette.primary.main,
                "&:hover": {
                  cursor: "pointer",
                  color: palette.primary.light,
                },
              }}
            >
              {isLogin
                ? "¿No tienes una cuenta? Registrate aqui."
                : "¿Ya tienes una cuenta? Inicia sesion aqui."}
            </Typography>
          </Box>
        </form>
      )}
    </Formik>
  );
};

export default Form;
