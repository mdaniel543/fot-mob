import React from "react";
import {
  Box,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../../components/Header";
import { useCreateNoticiaMutation } from "../../../app/api/func/Noticias";
import Swal from "sweetalert2";
import { useGetEquiposQuery } from "../../../app/api/datosDeportivos/apiEquipo";

const initialValues = {
  descripcion: "",
  id_equipo: "",
};

const CreateNoticia = ({ usuario }) => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const { data: equipos = [] } = useGetEquiposQuery();
  const [createNoticia] = useCreateNoticiaMutation();

  const handleFormSubmit = async (values, onSubmitProps) => {
    setIsSubmitting(true);
    const { descripcion, id_equipo } = values;
    try {
      const response = await createNoticia({
        descripcion,
        id_equipo,
        id_usuario: usuario?.id_usuario,
      });
      if (response.data) {
        Swal.fire({
          title: "Noticia creada con exito",
          icon: "success",
          confirmButtonText: "Aceptar",
        });
        onSubmitProps.resetForm();
      }
    } catch (error) {
      Swal.fire({
        title: "Error al crear la noticia",
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
    setIsSubmitting(false);
  };

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Crear Noticia"
        subtitle="Crear una nueva noticia para el equipo"
      />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="area"
                multiline
                rows={4}
                label="Descripcion"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.descripcion}
                name="descripcion"
                error={!!touched.descripcion && !!errors.descripcion}
                helperText={touched.descripcion && errors.descripcion}
                sx={{ gridColumn: "span 4" }}
              />

              <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                <InputLabel id="demo-simple-select-label-19" variant="filled">
                  Equipo
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-19"
                  label="Equipo"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_equipo}
                  name="id_equipo"
                  error={
                    Boolean(touched.id_equipo) && Boolean(errors.id_equipo)
                  }
                  helperText={touched.id_equipo && errors.id_equipo}
                >
                  {equipos.map((equipo) => (
                    <MenuItem value={equipo.id_equipo}>
                      {equipo.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={isSubmitting}
              >
                Crear Nueva Noticia
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
};

const checkoutSchema = yup.object().shape({
  descripcion: yup.string().required("Campo requerido"),
  id_equipo: yup.string().required("Campo requerido"),
});

export default CreateNoticia;
