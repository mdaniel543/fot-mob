import React from "react";
import { Box } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";

const ListNoticias = ({
  data = [
    { id_noticia: "1", hola: "jkfjsd", adios: "kjf" },
    { id_noticia: "2", hola: "fsdf", adios: "dfsf" },
  ],
  isFetching = true,
}) => {
  //const fild = (data[0] && Object.keys(data[0])) || [];

  const columns = [
    { field: "id_noticia", headerName: "ID", flex: 0.5 },
    { field: "equipo", headerName: "Equipo", flex: 1 },
    { field: "usuario", headerName: "Usuario", flex: 1 },
    { field: "descripcion", headerName: "Descripcion", flex: 2.5 },
  ]

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Noticias"
        subtitle="Aqui podras ver las noticias que se han publicado en la plataforma"
      />
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_noticia}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </Box>
  );
};

export default ListNoticias;
