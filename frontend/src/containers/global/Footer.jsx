import { useTheme } from "@emotion/react";
import { Box, Typography } from "@mui/material";
import { shades } from "../../style/theme";

function Footer() {
  const {
    palette: { neutral },
  } = useTheme();
  return (
    <Box marginTop="70px" padding="40px 0" backgroundColor={neutral.light}>
      <Box
        width="80%"
        margin="auto"
        display="flex"
        justifyContent="space-between"
        flexWrap="wrap"
        rowGap="30px"
        columnGap="clamp(20px, 30px, 40px)"
      >
        <Box width="clamp(50%, 30%, 40%)">
          <Typography
            variant="h4"
            fontWeight="bold"
            mb="30px"
            color={shades.primary[100]}
          >
            Acerca de
          </Typography>
          <div>
            <Typography mb="30px"
            justifyContent="space-between"
            >
            Somos una empresa de tecnología que busca brindar una experiencia
            única a los fanáticos del fútbol.
            Obtenga puntajes en vivo, estadísticas detalladas y noticias
            personalizadas para cientos de competencias en todo el mundo, hemos
            tomado ejemplos de la industria del entretenimiento para reinventar
            cómo los fanáticos de la Generación Z consumen fútbol: contenido
            personalizado, cuando y donde lo deseen.
            Seguimos trabajando todos los días para crear 
            una herramienta profunda y fácil de usar para mantenerse 
            al día con el mundo del fútbol.
            </Typography>
          </div>
        </Box>
        <Box width="clamp(20%, 25%, 30%)">
          <Typography variant="h4" fontWeight="bold" mb="30px">
            Contacto
          </Typography>
          <Typography mb="30px">
            Universidad de San Carlos de Guatemala Facultad de Ingeniera
          </Typography>
          <Typography mb="30px" sx={{ wordWrap: "break-word" }}>
            Grupo 8 | Software Avanzado
          </Typography>
          <Typography mb="30px">Diciembre 2022</Typography>
        </Box>
      </Box>
    </Box>
  );
}

export default Footer;
