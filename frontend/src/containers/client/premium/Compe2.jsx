import React from "react";
import {
  Box,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  Button,
} from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";
import {
  useGetEstadisticasCompetenciaQuery,
  useGetEstadisticasCompetencia2Query,
} from "../../../app/api/stats/apiEstadisticas";
import { useEffect } from "react";

const Compe2 = ({ lista, title, subtitle, tipo }) => {
  const [id, setId] = React.useState(0);
  const [data, setData] = React.useState([]);
  const [isFetching, setIsFetching] = React.useState(false);
  const [rows, setRows] = React.useState([]);
  const [columns, setColumns] = React.useState([]);

  const { data: data1 = [], isFetching: isFetching1 } =
    useGetEstadisticasCompetenciaQuery(id);

  const { data: data2 = [], isFetching: isFetching2 } =
    useGetEstadisticasCompetencia2Query(id);

  const handleChange = (event) => {
    setId(event.target.value);
  };

  useEffect(() => {
    setRows(
      data?.map((item, i) => ({
        ...item,
        id: i,
      }))
    );
    const fild = (data[0] && Object.keys(data[0])) || [];
    setColumns(
      fild?.map((item) => {
        return {
          field: item,
          headerName: item,
          flex: 1,
          type:
            item === "edad" ||
            "gol_loc" ||
            "gol_vis" ||
            "capacidad" ||
            "antiguedad"
              ? "number"
              : "string",
        };
      })
    );
  }, [data, id]); 

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title={title} subtitle={subtitle} />
      <FormControl
        fullWidth
        sx={{ gridColumn: "span 4", marginBottom: "15px" }}
      >
        <InputLabel id="demo-simple-select-label-19" variant="filled">
          {tipo === 2 ? "Equipo" : "Competencia"}
        </InputLabel>
        <Select
          labelId="demo-simple-select-label-19"
          label={tipo === 2 ? "Equipo" : "Competencia"}
          variant="filled"
          id="demo-simple-select-1"
          onChange={handleChange}
          value={id}
          name="id"
        >
          {lista?.map((item) => (
            <MenuItem value={tipo === 2 ? item.id_equipo : item.id_competencia}>
              {item.nombre}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Button
        variant="contained"
        sx={{ gridColumn: "span 4", marginBottom: "15px" }}
        onClick={() => {
          setData(tipo === 1 ? data1 : data2);
        }}
      >
        Peticion
      </Button>
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}
      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            rows={rows}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </Box>
  );
};

export default Compe2;
