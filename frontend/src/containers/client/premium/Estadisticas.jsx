import React from "react";
import { Box } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";

const Estadisticas = ({ data = [], isFetching = false, title, subtitle }) => {
  const rows = data.map((item, i) => ({
    ...item,
    id: i,
  }));

  const fild = (data[0] && Object.keys(data[0])) || [];

  const columns = fild?.map((item) => {
    return {
      field: item,
      headerName: item.toUpperCase(),
      flex: 1,
      type:
        item === "edad" || item === "antiguedad" || item === "capacidad"
          ? "number"
          : "string",
    };
  });

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title={title} subtitle={subtitle} />
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            rows={rows}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </Box>
  );
};

export default Estadisticas;
