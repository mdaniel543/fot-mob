import React from "react";
import { Box, MenuItem, Select, FormControl, InputLabel } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";
import { useGetEstadisticas3Query } from "../../../app/api/stats/apiEstadisticas";

const Deportistas = ({ deportista = [], title, subtitle, tipo }) => {
  const [id_deportista, setId_deportista] = React.useState(0);

  const { data = [], isFetching } = useGetEstadisticas3Query({
    id: id_deportista,
    tipo: tipo
  });

  const rows = data?.map((item, i) => ({
    ...item,
    id: i,
  }));
  const fild = (data[0] && Object.keys(data[0])) || [];

  const columns = fild?.map((item) => {
    return {
      field: item,
      headerName: item,
      flex: 1,
      type:
        item === "edad" || "gol_loc" || "gol_vis" || "capacidad" || "antiguedad"
          ? "number"
          : "string",
    };
  });

  const handleChange = (event) => {
    setId_deportista(event.target.value);
  };

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title={title} subtitle={subtitle} />
      <FormControl
        fullWidth
        sx={{ gridColumn: "span 4", marginBottom: "15px" }}
      >
        <InputLabel id="demo-simple-select-label-19" variant="filled">
          {tipo === 2 ? "Tecnico" : "Jugador"}
        </InputLabel>
        <Select
          labelId="demo-simple-select-label-19"
          label={tipo === 2 ? "Tecnico" : "Jugador"}
          variant="filled"
          id="demo-simple-select-1"
          onChange={handleChange}
          value={id_deportista}
          name="id_deportista"
        >
          {deportista?.map((item) => (
            <MenuItem value={tipo === 2 ? item.id_tecnico : item.id_jugador}>
              {item.nombre}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            rows={rows}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </Box>
  );
};

export default Deportistas;
