import React, { useState } from "react";
import {
  Box,
  TextField,
  Button,
  Modal,
  Typography,
  IconButton,
} from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";
import { useGetQuinielaQuery } from "../../../app/api/stats/apiQuiniela";
import Swal from "sweetalert2";
import { usePutQuinielaMutation } from "../../../app/api/stats/apiQuiniela";
import EditIcon from "@mui/icons-material/Edit";

const Quiniela = ({ id_usuario }) => {
  const [open, setOpen] = React.useState(false);
  const [dataModal, setDataModal] = React.useState({});
  const {
    data = [],
    isFetching,
  } = useGetQuinielaQuery(id_usuario);

  const columns = [
    { field: "id_quiniela", headerName: "ID", flex: 0.5 },
    { field: "equipo_local", headerName: "Equipo Local", flex: 1 },
    { field: "equipo_visit", headerName: "Equipo Visitante", flex: 1 },
    { field: "gol_local", headerName: "Goles Local", flex: 1 },
    { field: "gol_visit", headerName: "Goles Visitante", flex: 1 },
    {
      field: "Acciones",
      headerName: "Acciones",
      flex: 0.8,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            style={{ marginRight: 16 }}
            onClick={() => {
              setOpen(true);
              setDataModal(params.row);
            }}
          >
            <EditIcon />
          </IconButton>
        </strong>
      ),
    },
  ];

  return (
    <>
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Quinielas"
        subtitle="Aquí puedes ver las quinielas que has creado"
      />
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            rows={data}
            getRowId={(row) => row.id_quiniela}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </Box>
    <ModalUpdate data={dataModal} open={open} setOpen={setOpen} />
    </>
  );
};

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "50%",
  height: "60%",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function ModalUpdate({ data = {}, open, setOpen }) {
  const handleClose = () => setOpen(false);
  const [datos, setDatos] = useState();

  const [putQuiniela] = usePutQuinielaMutation();

  const handle = async () => {
    const datas = {
      id_quiniela: data.id_quiniela,
      gol_loc: datos.gol_loc,
      gol_vis: datos.gol_vis,
    };
    await putQuiniela(datas)
      .unwrap()
      .then((res) => {
        Swal.fire({
          icon: "success",
          title: "Quiniela Guardada",
          showConfirmButton: false,
          timer: 1500,
        });
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Error al guardar la quiniela",
          showConfirmButton: false,
          timer: 1500,
        });
      });

    handleClose();
  };

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={style}
          gap="30px"
          display="grid"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
        >
          <Typography
            sx={{ gridColumn: "span 4" }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Quiniela de Partido
          </Typography>

          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Equipo Local"
            variant="outlined"
            value={data.equipo_local}
            disabled
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Equipo Visitante"
            variant="outlined"
            value={data.equipo_visitante}
            disabled
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Goles Local"
            variant="outlined"
            value={datos?.gol_loc}
            onChange={(e) => setDatos({ ...datos, gol_loc: e.target.value })}
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Goles Visitante"
            variant="outlined"
            value={datos?.gol_vis}
            onChange={(e) => setDatos({ ...datos, gol_vis: e.target.value })}
          />

          <Box
            sx={{ gridColumn: "span 4" }}
            display="flex"
            justifyContent="flex-end"
          >
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: "15px" }}
              onClick={handle}
            >
              Actualizar Quiniela
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}

export default Quiniela;
