import React from "react";
import { Box, IconButton, Typography } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import CircularProgress from "@mui/material/CircularProgress";
import {
  useUpdateUsuarioMutation,
  useUpdateUsuarioEstadoMutation,
} from "../../../app/api/users/apiUsuario";
import ModalEdit from "../../../components/ModalEdit";
import ModalEstado from "../../../components/ModalEstado";
import Swal from "sweetalert2";
import AcUnitIcon from "@mui/icons-material/AcUnit";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import { useSelector } from "react-redux";

const ListUsers = ({ data = [], isFetching = true }) => {
  const [updateUsuario] = useUpdateUsuarioMutation();
  const [updateEstadoUsuario] = useUpdateUsuarioEstadoMutation();

  const num_grupo = useSelector((state) => state.sesion.num_grupo);

  const [open, setOpen] = React.useState(false);
  const [dataModal, setDataModal] = React.useState({});

  const [openEstado, setOpenEstado] = React.useState(false);

  const columns = [
    { field: "id_usuario", headerName: "ID", flex: 0.5, type: "number" },
    {
      field: "nombre",
      headerName: "Nombre",
      flex: 1,
      cellClassnombre: "nombre-column--cell",
    },
    {
      field: "apellido",
      headerName: "Apellido",
      flex: 1,
    },
    {
      field: "correo",
      headerName: "Email",
      flex: 1.4,
      cellClassnombre: "nombre-column--cell",
    },
    {
      field: "telefono",
      headerName: "Telefono",
      flex: 1,
    },
    {
      field: "genero",
      headerName: "Genero",
      flex: 1,
    },
    {
      field: "fecha_nac",
      headerName: "Fecha Nacimiento",
      flex: 1,
    },
    {
      field: "pais",
      headerName: "Pais",
      flex: 1,
    },
    {
      field: "estado",
      headerName: "Estado",
      flex: 1.3,
      renderCell: ({ row: { estado } }) => (
        <Box
          width="100%"
          m="0 auto"
          p="5px"
          display="flex"
          justifyContent="center"
          backgroundColor={
            estado === "De alta"
              ? shades.greenAccent[600]
              : estado === "De baja"
              ? shades.redAccent[600]
              : shades.blueAccent[600]
          }
          borderRadius="4px"
        >
          {estado === "De alta" ? (
            <ArrowUpwardIcon />
          ) : estado === "De baja" ? (
            <ArrowDownwardIcon />
          ) : (
            <AcUnitIcon />
          )}
          <Typography sx={{ ml: "5px" }}>{estado}</Typography>
        </Box>
      ),
    },
    {
      field: "Acciones",
      headerName: "Acciones",
      flex: 1.2,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            style={{ marginRight: 16 }}
            onClick={() => {
              setOpen(true);
              setDataModal(params.row);
            }}
          >
            <EditIcon />
          </IconButton>
          <IconButton
            variant="contained"
            color="danger"
            size="small"
            onClick={() => {
              setOpenEstado(true);
              setDataModal(params.row);
            }}
          >
            <DeleteIcon />
          </IconButton>
        </strong>
      ),
    },
  ];

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Clientes"
        subtitle="Lista de clientes registrados en el sistema"
      />
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_usuario}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
      <ModalEdit
        open={open}
        setOpen={setOpen}
        data={dataModal}
        setData={setDataModal}
        handleEdit={async (data) => {
          setOpen(false);
          Swal.fire("Actualizando...");
          console.log(data);
          updateUsuario({
            ...data,
            num_grupo: num_grupo,
          })
            .unwrap()
            .then((res) => {
              Swal.fire(
                "Actualizado",
                "El cliente ha sido actualizado",
                "success"
              );
            })
            .catch((err) => {
              Swal.fire("Error", "No se pudo actualizar el cliente", "error");
            });
        }}
      />
      <ModalEstado
        open={openEstado}
        setOpen={setOpenEstado}
        data={dataModal}
        setData={setDataModal}
        handleEdit={async (data) => {
          setOpenEstado(false);
          Swal.fire("Actualizando...");
          console.log(data);
          await updateEstadoUsuario({
            ...data,
            num_grupo: num_grupo
          })
            .unwrap()
            .then((res) => {
              Swal.fire(
                "Actualizado",
                "El cliente ha sido actualizado su estado",
                "success"
              );
            })
            .catch((err) => {
              Swal.fire("Error", "No se pudo actualizar el cliente", "error");
            });
        }}
      />
    </Box>
  );
};

export default ListUsers;
