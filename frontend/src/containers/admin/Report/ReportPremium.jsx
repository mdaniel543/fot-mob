import React from "react";
import { Box } from "@mui/material";
import { DataGridPremium, GridToolbar } from "@mui/x-data-grid-premium";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";

const ReportesPremium = ({
  data = [],
  isFetching = false,
  title,
  subtitle,
}) => {
  const rows = data?.map((item, i) => ({
    ...item,
    id: i,
  }));

  const columns = [
    { field: "id", headerName: "ID", width: 70 },
    {
      field: "nombre",
      headerName: "Nombre",
      type: "string",
      width: 130,
    },
    {
      field: "membresia",
      headerName: "Membresia",
      flex: 1,
    },
    {
      field: "cantidad_membresias",
      headerName: "Cantidad de Membresias",
      flex: 1.2,
      type: "number",
    },
    {
      field: "correo",
      headerName: "Correo",
      flex: 1,
    },
    {
      field: "pais",
      headerName: "Pais",
      flex: 1,
    },
    {
      field: "genero",
      headerName: "Genero",
      flex: 1,
    },
    {
      field: "fecha_nac",
      headerName: "Fecha",
      flex: 1,
    },
    {
      field: "edad",
      headerName: "Edad",
      flex: 0.5,
      type: "number",
    },
    {
      field: "gasto_membresias",
      headerName: "Gasto Membresia",
      flex: 1,
      type: "number",
    },
  ];

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title={title} subtitle={subtitle} />
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGridPremium
            rows={rows}
            isRowSelectable={() => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </Box>
  );
};

export default ReportesPremium;
