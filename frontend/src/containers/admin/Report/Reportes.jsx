import React from "react";
import {
  Box,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  Button,
} from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";
import {
  useGetReportesEmpleadoQuery,
  useGetReportesEmpleado2Query,
  useGetReportesCliente2Query,
} from "../../../app/api/report/reportesAdmin";
import { useSelector } from "react-redux";
import { useEffect } from "react";

const Reportes = ({ title, subtitle, equipos = [], validate = {}, datos = [] }) => {
  const url = useSelector((state) => state.sesion.url);

  const [id_equipo, setId_equipo] = React.useState(0);
  const [id_orden, setId_orden] = React.useState(0);
  const [cambio, setCambio] = React.useState(false);
  const [rows, setRows] = React.useState([]);
  const [columns, setColumns] = React.useState([]);
  const [data, setData] = React.useState([]);

  const { data: data1 } = useGetReportesCliente2Query(id_equipo);

  const { data: data2 } = useGetReportesEmpleadoQuery({
    orden: id_orden,
    url: url
  });
  const { data: data3 } = useGetReportesEmpleado2Query({ id_equipo, id_orden });

  const handelSubmit = () => {
    setCambio(!cambio);
    if (validate?.uno) {
      if (validate?.dos) {
        setData(data3?.data);
      } else {
        setData(data2?.data);
      }
    } else {
      if (validate?.dos) {
        setData(data1);
      }else{
        setData(datos);
      }
    }
  };

  useEffect(() => {
    setRows(data?.map((item, i) => ({ ...item, id: i })));
    const fields = (data[0] && Object.keys(data[0])) || [];
    setColumns(
      fields?.map((item) => {
        return {
          field: item,
          headerName: item,
          flex: 1,
          type:
            item === "edad" ||
            item === "gol_loc" ||
            item === "gol_vis" ||
            item === "capacidad" ||
            item === "antiguedad"
              ? "number"
              : "string",
        };
      })
    );
  }, [data]);

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title={title} subtitle={subtitle} />
      {validate?.uno && (
        <FormControl
          fullWidth
          sx={{ gridColumn: "span 4", marginBottom: "15px" }}
        >
          <InputLabel id="demo-simple-select-label-19" variant="filled">
            Orden
          </InputLabel>
          <Select
            labelId="demo-simple-select-label-19"
            label="Orden"
            variant="filled"
            id="demo-simple-select-1"
            onChange={(event) => setId_orden(event.target.value)}
            value={id_orden}
            name="id_orden"
          >
            <MenuItem value={2}>Ascendente</MenuItem>
            <MenuItem value={1}>Descendente</MenuItem>
          </Select>
        </FormControl>
      )}
      {validate?.dos && (
        <FormControl
          fullWidth
          sx={{ gridColumn: "span 4", marginBottom: "15px" }}
        >
          <InputLabel id="demo-simple-select-label-19" variant="filled">
            Equipo
          </InputLabel>
          <Select
            labelId="demo-simple-select-label-19"
            label="Equipo"
            variant="filled"
            id="demo-simple-select-1"
            onChange={(event) => setId_equipo(event.target.value)}
            value={id_equipo}
            name="id_equipo"
          >
            {equipos.map((item) => (
              <MenuItem key={item.id_equipo} value={item.id_equipo}>
                {item.nombre}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}
      <Button
        variant="contained"
        sx={{ gridColumn: "span 4", marginBottom: "15px" }}
        onClick={handelSubmit}
      >
        Peticion
      </Button>
      <DataTable columns={columns} rows={rows} isFetching={!data} />
    </Box>
  );
};

export default Reportes;

function DataTable({ columns, rows, isFetching }) {
  return (
    <>
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            rows={rows}
            isRowSelectable={() => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </>
  );
}
