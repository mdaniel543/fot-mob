import React from "react";
import { Box } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import Header from "../../components/Header";
import { shades } from "../../style/theme";
import CircularProgress from "@mui/material/CircularProgress";
import { useGetPrediccionesQuery } from "../../app/api/Algoritmo/apiPrediccion";
import { useSelector } from "react-redux";

export default function ListPredict() {
  const { predict } = useSelector((state) => state.predict);

  const { data = [], isFetching } = useGetPrediccionesQuery(predict);

  const columns = [
    { field: "id_prediccion", headerName: "ID", flex: 1 },
    { field: "equipo_loc", headerName: "Equipo Local", flex: 1.2 },
    { field: "equipo_vis", headerName: "Equipo Visitante", flex: 1.2 },
    { field: "gol_loc", headerName: "Goles Local", flex: 1 },
    { field: "gol_vis", headerName: "Goles Visitante", flex: 1 },
  ];

  return (
    <>
      <Box m="20px" sx={{ marginTop: "-25px" }}>
        <Header
          title="Predicciones"
          subtitle={
            data.length > 0
              ? "Listado de predicciones"
              : "No hay predicciones o no ha seleccionado el partido en el apartado de partidos"
          }
        />
        {isFetching && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "60vh",
            }}
          >
            <CircularProgress />
          </Box>
        )}

        {!isFetching && (
          <Box
            m="40px 0 -10 0"
            height="75vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
              },
              "& .nombre-column--cell": {
                color: shades.primary[100],
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: shades.primary[600],
                borderBottom: "none",
                color: shades.grey[900],
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: shades.primary[400],
              },
              "& .MuiDataGrid-footerContainer": {
                borderTop: "none",
                backgroundColor: shades.primary[600],
                color: shades.grey[900],
              },
              "& .MuiCheckbox-root": {
                color: `${shades.grey[100]} !important`,
              },
              "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                color: `${shades.grey[100]} !important`,
              },
            }}
          >
            <DataGrid
              getRowId={(row) => row.id_prediccion}
              rows={data}
              isRowSelectable={() => false}
              columns={columns}
              components={{ Toolbar: GridToolbar }}
            />
          </Box>
        )}
      </Box>
    </>
  );
}
