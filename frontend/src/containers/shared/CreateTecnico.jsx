import React from 'react'
import { Box, Button, TextField, MenuItem, Select, FormControl, InputLabel } from '@mui/material'
import { Formik } from 'formik'
import * as yup from 'yup'
import useMediaQuery from '@mui/material/useMediaQuery'
import InputAdornment from '@mui/material/InputAdornment'
import TodayIcon from '@mui/icons-material/Today'
import Swal from 'sweetalert2'
import { useCreateTecnicoMutation } from '../../app/api/datosDeportivos/apiTecnico'
import Header from '../../components/Header'
import { useGetPaisesQuery } from '../../app/api/func/Pais'

const initialValues = {
  fecha_nac: '',
  nombre: '',
}

const CreateTecnico = () => {
  const isNonMobile = useMediaQuery('(min-width:600px)')
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const [CreateArbitro] = useCreateTecnicoMutation()

  const { data: paises = [] } = useGetPaisesQuery()

  const handleFormSubmit = async (values) => {
    setIsSubmitting(true)
    const data = {
      ...values,
    }
    console.log(data)
    await CreateArbitro(data)
      .unwrap()
      .then((res) => {
        Swal.fire({
          title: 'Tecnico Creado',
          text: 'El Tecnico ha sido creado con exito',
          icon: 'success',
          confirmButtonText: 'Ok',
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'Error',
          text: 'Ha ocurrido un error al crear el Tecnico',
          icon: 'error',
          confirmButtonText: 'Ok',
        })
      })
    // borrar los valores del formulario
    setIsSubmitting(false)
    values = initialValues
  }

  return (
    <Box m="20px" sx={{ marginTop: '-25px' }}>
      <Header title="Crear Tecnico" subtitle="Crear un nuevo Tecnico" />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({ values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                '& > div': { gridColumn: isNonMobile ? undefined : 'span 4' },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Nombres"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.nombre}
                name="nombre"
                error={!!touched.nombre && !!errors.nombre}
                helperText={touched.nombre && errors.nombre}
                sx={{ gridColumn: 'span 2' }}
              />
              <TextField
                label="Fecha de nacimiento"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.fecha_nac}
                name="fecha_nac"
                error={Boolean(touched.fecha_nac) && Boolean(errors.fecha_nac)}
                helperText={touched.fecha_nac && errors.fecha_nac}
                sx={{ gridColumn: 'span 2' }}
                fullWidth
                format="dd-MM-yyyy"
                type="date"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <TodayIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button type="submit" color="primary" variant="contained" disabled={isSubmitting}>
                Crear Nuevo Tecnico
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: '2rem' }}></div>
    </Box>
  )
}

const checkoutSchema = yup.object().shape({
  nombre: yup.string().required('Campo requerido'),
  fecha_nac: yup.string().required('Campo requerido'),
})

export default CreateTecnico
