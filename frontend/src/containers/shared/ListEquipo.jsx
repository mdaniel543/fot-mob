import React from 'react'
import { Box, IconButton } from '@mui/material'
import { DataGrid, GridToolbar } from '@mui/x-data-grid'
import { shades } from '../../style/theme'
import Header from '../../components/Header'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import CircularProgress from '@mui/material/CircularProgress'
import Swal from 'sweetalert2'
import {
  useDeleteEquipoMutation,
  useUpdateEquipoMutation,
} from '../../app/api/datosDeportivos/apiEquipo'
import ModalEquipo from '../../components/ModalEquipo'
const ListEquipo = ({ data = [], isFetching = true }) => {
  const [updateUsuario] = useUpdateEquipoMutation()
  const [deleteEquipo] = useDeleteEquipoMutation()

  const [open, setOpen] = React.useState(false)
  const [dataModal, setDataModal] = React.useState({})

  const columns = [
    { field: 'id_equipo', headerName: 'ID', flex: 0.5 },
    {
      field: 'nombre',
      headerName: 'Nombre',
      flex: 1.2,
      cellClassnombre: 'nombre-column--cell',
    },
    {
      field: 'pais',
      headerName: 'Pais',
      flex: 1.4,
      cellClassnombre: 'nombre-column--cell',
    },
    {
      field: 'fecha_fun',
      headerName: 'Fecha',
      flex: 1.4,
      cellClassnombre: 'nombre-column--cell',
    },
    {
      field: 'tipo',
      headerName: 'Tipo',
      flex: 1.4,
      cellClassnombre: 'nombre-column--cell',
    },
    {
      field: 'Acciones',
      headerName: 'Acciones',
      flex: 1.2,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            style={{ marginRight: 16 }}
            onClick={() => {
              setOpen(true)
              setDataModal(params.row)
            }}
          >
            <EditIcon />
          </IconButton>
          <IconButton
            variant="contained"
            color="danger"
            size="small"
            onClick={() => {
              Swal.fire({
                title: '¿Estas seguro?',
                text: 'No podras revertir esta accion!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar!',
              }).then((result) => {
                if (result.isConfirmed) {
                  deleteEquipo(params.row.id_equipo)
                    .unwrap()
                    .then(() => {
                      Swal.fire('Eliminado!', 'El equipo ha sido eliminado.', 'success')
                    })
                }
              })
            }}
          >
            <DeleteIcon />
          </IconButton>
        </strong>
      ),
    },
  ]

  return (
    <Box m="20px" sx={{ marginTop: '-25px' }}>
      <Header title="Equipos" subtitle="Lista de equipos registrados en el sistema" />
      {isFetching && (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '60vh',
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            '& .MuiDataGrid-root': {
              border: 'none',
            },
            '& .MuiDataGrid-cell': {
              borderBottom: 'none',
            },
            '& .nombre-column--cell': {
              color: shades.primary[100],
            },
            '& .MuiDataGrid-columnHeaders': {
              backgroundColor: shades.primary[600],
              borderBottom: 'none',
              color: shades.grey[900],
            },
            '& .MuiDataGrid-virtualScroller': {
              backgroundColor: shades.primary[400],
            },
            '& .MuiDataGrid-footerContainer': {
              borderTop: 'none',
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            '& .MuiCheckbox-root': {
              color: `${shades.grey[100]} !important`,
            },
            '& .MuiDataGrid-toolbarContainer .MuiButton-text': {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_equipo}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
      <ModalEquipo
        open={open}
        setOpen={setOpen}
        data={dataModal}
        setData={setDataModal}
        handleEdit={async (data) => {
          setOpen(false)
          Swal.fire('Actualizando...')
          console.log(data)
          updateUsuario(data)
            .unwrap()
            .then((res) => {
              Swal.fire('Actualizado', 'El equipo ha sido actualizado', 'success')
            })
            .catch((err) => {
              Swal.fire('Error', 'No se pudo actualizar el equipo', 'error')
            })
        }}
      />
      {/* <ModalEstado
        open={openEstado}
        setOpen={setOpenEstado}
        data={dataModal}
        setData={setDataModal}
        handleEdit={async (data) => {
          setOpenEstado(false)
          Swal.fire('Actualizando...')
          console.log(data)
          await updateEstadoUsuario(data)
            .unwrap()
            .then((res) => {
              Swal.fire('Actualizado', 'El cliente ha sido actualizado su estado', 'success')
            })
            .catch((err) => {
              Swal.fire('Error', 'No se pudo actualizar el cliente', 'error')
            })
        }}
      /> */}
    </Box>
  )
}

export default ListEquipo
