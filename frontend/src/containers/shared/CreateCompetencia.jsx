import React from 'react'
import {
  Box,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  useTheme,
  Typography,
} from '@mui/material'
import { Formik } from 'formik'
import * as yup from 'yup'
import useMediaQuery from '@mui/material/useMediaQuery'
import InputAdornment from '@mui/material/InputAdornment'
import TodayIcon from '@mui/icons-material/Today'
import Swal from 'sweetalert2'
import { useCreateCompetenciaMutation } from '../../app/api/datosDeportivos/apiCompetencia'
import Header from '../../components/Header'
import { useGetPaisesQuery } from '../../app/api/func/Pais'

const initialValues = {
  nombre: '',
  fecha: '',
  id_pais: '',
  estado: 3,
}

const CreateCompetencia = () => {
  const isNonMobile = useMediaQuery('(min-width:600px)')
  const { palette } = useTheme()
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const [createUsuario] = useCreateCompetenciaMutation()

  const { data: paises = [] } = useGetPaisesQuery()

  const handleFormSubmit = async (values) => {
    setIsSubmitting(true)
    const data = {
      ...values,
      id_pais: parseInt(values.id_pais),
      estado: parseInt(values.estado),
    }
    console.log(data)
    await createUsuario(data)
      .unwrap()
      .then((res) => {
        Swal.fire({
          title: 'Competencia Creada',
          text: 'La competencia ha sido creada con exito',
          icon: 'success',
          confirmButtonText: 'Ok',
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'Error',
          text: 'Ha ocurrido un error al crear la competencia',
          icon: 'error',
          confirmButtonText: 'Ok',
        })
      })
    // borrar los valores del formulario
    setIsSubmitting(false)
    values = initialValues
  }

  return (
    <Box m="20px" sx={{ marginTop: '-25px' }}>
      <Header title="Crear Competencia" subtitle="Crear una nueva competencia" />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({ values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                '& > div': { gridColumn: isNonMobile ? undefined : 'span 4' },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Nombres"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.nombre}
                name="nombre"
                error={!!touched.nombre && !!errors.nombre}
                helperText={touched.nombre && errors.nombre}
                sx={{ gridColumn: 'span 2' }}
              />
              <FormControl fullWidth sx={{ gridColumn: 'span 2' }}>
                <InputLabel id="demo-simple-select-label" variant="filled">
                  Estado Competencia
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  label="Genero"
                  variant="filled"
                  id="demo-simple-select"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.estado}
                  name="id_genero"
                  error={Boolean(touched.estado) && Boolean(errors.estado)}
                  helperText={touched.estado && errors.estado}
                >
                  <MenuItem value={1}>En Progreso</MenuItem>
                  <MenuItem value={2}>Finalizada</MenuItem>
                  <MenuItem value={3}>Programada</MenuItem>
                </Select>
              </FormControl>
              <TextField
                label="Fecha de realizamiento"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.fecha}
                name="fecha"
                error={Boolean(touched.fecha) && Boolean(errors.fecha)}
                helperText={touched.fecha && errors.fecha}
                sx={{ gridColumn: 'span 2' }}
                fullWidth
                format="dd-MM-yyyy"
                type="date"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <TodayIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
              <FormControl fullWidth sx={{ gridColumn: 'span 2' }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Pais
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Pais"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_pais}
                  name="id_pais"
                  error={Boolean(touched.id_pais) && Boolean(errors.id_pais)}
                  helperText={touched.id_pais && errors.id_pais}
                >
                  {paises.map((pais) => (
                    <MenuItem value={pais.id_pais}>{pais.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button type="submit" color="primary" variant="contained" disabled={isSubmitting}>
                Crear Nueva Competencia
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: '2rem' }}></div>
    </Box>
  )
}

const checkoutSchema = yup.object().shape({
  nombre: yup.string().required('Campo requerido'),
  fecha: yup.string().required('Campo requerido'),
  //id_pais: yup.string().required("Campo requerido"),
  estado: yup.string().required('Campo requerido'),
})

export default CreateCompetencia
