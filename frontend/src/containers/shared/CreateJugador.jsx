import React from "react";
import {
  Box,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../components/Header";
import InputAdornment from "@mui/material/InputAdornment";
import TodayIcon from "@mui/icons-material/Today";
import { useCreateJugadorMutation } from "../../app/api/datosDeportivos/apiJugadores";
import { useGetPaisesQuery } from "../../app/api/func/Pais";
import { useGetPosicionesQuery } from "../../app/api/func/Posicion";
import Swal from "sweetalert2";

const initialValues = {
  nombre: "",
  fecha_nac: "",
  id_pais: "",
  id_posicion: "",
};

const CreateJugador = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isSubmitting, setIsSubmitting] = React.useState(false);

  const [CreateJugador] = useCreateJugadorMutation();

  const { data: paises = [] } = useGetPaisesQuery();

  const { data: posiciones = [] } = useGetPosicionesQuery();

  const handleFormSubmit = async (values, onSubmitProps) => {
    setIsSubmitting(true);
    const data = {
      ...values,
      id_pais: parseInt(values.id_pais),
      id_posicion: parseInt(values.id_posicion),
    };
    console.log(data);
    await CreateJugador(data)
      .unwrap()
      .then((res) => {
        Swal.fire({
          title: "Jugador Creado",
          text: "El Jugador ha sido creado con exito",
          icon: "success",
          confirmButtonText: "Ok",
        });
        onSubmitProps.resetForm();
      })
      .catch((err) => {
        Swal.fire({
          title: "Error",
          text: "Ha ocurrido un error al crear el juagador",
          icon: "error",
          confirmButtonText: "Ok",
        });
      });
    // borrar los valores del formulario
    setIsSubmitting(false);
  };

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title="Crear Jugador" subtitle="Registrar un nuevo Jugador" />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Nombres"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.nombre}
                name="nombre"
                error={!!touched.nombre && !!errors.nombre}
                helperText={touched.nombre && errors.nombre}
                sx={{ gridColumn: "span 4" }}
              />

              <TextField
                label="Fecha de nacimiento"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.fecha_nac}
                name="fecha_nac"
                error={Boolean(touched.fecha_nac) && Boolean(errors.fecha_nac)}
                helperText={touched.fecha_nac && errors.fecha_nac}
                sx={{ gridColumn: "span 4" }}
                fullWidth
                format="dd-MM-yyyy"
                type="date"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <TodayIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
              <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Pais
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Pais"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_pais}
                  name="id_pais"
                  error={Boolean(touched.id_pais) && Boolean(errors.id_pais)}
                  helperText={touched.id_pais && errors.id_pais}
                >
                  {paises.map((pais) => (
                    <MenuItem value={pais.id_pais}>{pais.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Posicion
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Posicion"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_posicion}
                  name="id_posicion"
                  error={
                    Boolean(touched.id_posicion) && Boolean(errors.id_posicion)
                  }
                  helperText={touched.id_posicion && errors.id_posicion}
                >
                  {posiciones.map((posicion) => (
                    <MenuItem value={posicion.id_posicion}>
                      {posicion.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={isSubmitting}
              >
                Crear
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
};

const checkoutSchema = yup.object().shape({
  nombre: yup.string().required("Campo requerido"),
  fecha_nac: yup.string().required("Campo requerido"),
  id_pais: yup.string().required("Campo requerido"),
  id_posicion: yup.string().required("Campo requerido"),
});

export default CreateJugador;
