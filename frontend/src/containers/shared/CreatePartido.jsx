import React from "react";
import {
  Box,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../components/Header";
import InputAdornment from "@mui/material/InputAdornment";
import TodayIcon from "@mui/icons-material/Today";
import AccessTimeFilledRoundedIcon from "@mui/icons-material/AccessTimeFilledRounded";
import { useCreatePartidoMutation } from "../../app/api/datosDeportivos/apiPartido";
import { useGetCompetenciasQuery } from "../../app/api/datosDeportivos/apiCompetencia";
import { useGetEquiposQuery } from "../../app/api/datosDeportivos/apiEquipo";
import { useGetEstadiosQuery } from "../../app/api/datosDeportivos/ApiEstadio";
import { useGetArbitrosQuery } from "../../app/api/datosDeportivos/apiArbitro";

import Swal from "sweetalert2";

const initialValues = {
  publico: "",
  hora: "",
  fecha: "",
  equipo_loc: "",
  equipo_vis: "",
  estadio: "",
  arbitro: "",
  competencia: "",
};

const CreatePartido = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isSubmitting, setIsSubmitting] = React.useState(false);

  const [CreatePartido] = useCreatePartidoMutation();

  const { data: equipos = [] } = useGetEquiposQuery();
  const { data: estadios = [] } = useGetEstadiosQuery();
  const { data: arbitros = [] } = useGetArbitrosQuery();
  const { data: competencias = [] } = useGetCompetenciasQuery();

  const handleFormSubmit = async (values, onSubmitProps) => {
    setIsSubmitting(true);
    console.log(values);
    await CreatePartido(values)
      .unwrap()
      .then((res) => {
        Swal.fire({
          title: "Partido Creado",
          text: "El partido ha sido creado con exito",
          icon: "success",
          confirmButtonText: "Ok",
        });
        onSubmitProps.resetForm();
      })
      .catch((err) => {
        Swal.fire({
          title: "Error",
          text: "Ha ocurrido un error al crear el partido",
          icon: "error",
          confirmButtonText: "Ok",
        });
      });
    // borrar los valores del formulario
    setIsSubmitting(false);
  };

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title="Crear Partido" subtitle="Registrar un nuevo Partido" />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="number"
                label="Publico"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.publico}
                name="publico"
                error={!!touched.publico && !!errors.publico}
                helperText={touched.publico && errors.publico}
                sx={{ gridColumn: "span 2" }}
              />

              <TextField
                label="Fecha"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.fecha}
                name="fecha"
                error={Boolean(touched.fecha) && Boolean(errors.fecha)}
                helperText={touched.fecha && errors.fecha}
                sx={{ gridColumn: "span 2" }}
                fullWidth
                type="date"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <TodayIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
              <TextField
                label="Hora"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.hora}
                name="hora"
                error={Boolean(touched.hora) && Boolean(errors.hora)}
                helperText={touched.hora && errors.hora}
                sx={{ gridColumn: "span 2" }}
                fullWidth
                type="time"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <AccessTimeFilledRoundedIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
              <FormControl
                fullWidth
                variant="filled"
                sx={{ gridColumn: "span 2" }}
              >
                <InputLabel id="estadio">Estadio</InputLabel>
                <Select
                  labelId="estadio"
                  id="estadio"
                  name="estadio"
                  value={values.estadio}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={Boolean(touched.estadio) && Boolean(errors.estadio)}
                  helperText={touched.estadio && errors.estadio}
                >
                  {estadios.map((estadio) => (
                    <MenuItem
                      key={estadio.id_estadio}
                      value={estadio.id_estadio}
                    >
                      {estadio.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl
                fullWidth
                variant="filled"
                sx={{ gridColumn: "span 2" }}
              >
                <InputLabel id="equipo_loc">Equipo Local</InputLabel>
                <Select
                  labelId="equipo_loc"
                  id="equipo_loc"
                  name="equipo_loc"
                  value={values.equipo_loc}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={
                    Boolean(touched.equipo_loc) && Boolean(errors.equipo_loc)
                  }
                  helperText={touched.equipo_loc && errors.equipo_loc}
                >
                  {equipos.map((equipo) => (
                    <MenuItem key={equipo.id_equipo} value={equipo.id_equipo}>
                      {equipo.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl
                fullWidth
                variant="filled"
                sx={{ gridColumn: "span 2" }}
              >
                <InputLabel id="equipo_vis">Equipo Visitante</InputLabel>
                <Select
                  labelId="equipo_vis"
                  id="equipo_vis"
                  name="equipo_vis"
                  value={values.equipo_vis}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={
                    Boolean(touched.equipo_vis) && Boolean(errors.equipo_vis)
                  }
                  helperText={touched.equipo_vis && errors.equipo_vis}
                >
                  {equipos.map((equipo) => (
                    <MenuItem key={equipo.id_equipo} value={equipo.id_equipo}>
                      {equipo.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl
                fullWidth
                variant="filled"
                sx={{ gridColumn: "span 2" }}
              >
                <InputLabel id="competicion">Competicion</InputLabel>
                <Select
                  labelId="competicion"
                  id="competicion"
                  name="competencia"
                  value={values.competencia}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={
                    Boolean(touched.competencia) && Boolean(errors.competencia)
                  }
                  helperText={touched.competencia && errors.competencia}
                >
                  {competencias.map((competencia) => (
                    <MenuItem
                      key={competencia.id_competencia}
                      value={competencia.id_competencia}
                    >
                      {competencia.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl
                fullWidth
                variant="filled"
                sx={{ gridColumn: "span 2" }}
              >
                <InputLabel id="arbitro">Arbitro</InputLabel>
                <Select
                  labelId="arbitro"
                  id="arbitro"
                  name="arbitro"
                  value={values.arbitro}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={Boolean(touched.arbitro) && Boolean(errors.arbitro)}
                  helperText={touched.arbitro && errors.arbitro}
                >
                  {arbitros.map((arbitro) => (
                    <MenuItem
                      key={arbitro.id_arbitro}
                      value={arbitro.id_arbitro}
                    >
                      {arbitro.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={isSubmitting}
              >
                Crear
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
};

const checkoutSchema = yup.object().shape({
  fecha: yup.string().required("Campo requerido"),
  hora: yup.string().required("Campo requerido"),
  estadio: yup.string().required("Campo requerido"),
  equipo_loc: yup.string().required("Campo requerido"),
  equipo_vis: yup.string().required("Campo requerido"),
  competencia: yup.string().required("Campo requerido"),
  arbitro: yup.string().required("Campo requerido"),
  publico: yup.string().required("Campo requerido"),
});

export default CreatePartido;
