# Implementacion de pruebas

## Pruebas Unitarias

Las pruebas unitarias consisten en verificar el comportamiento de las unidades más pequeñas de su aplicación. Técnicamente, eso sería una clase o incluso un método de clase en los lenguajes orientados a objetos, y un procedimiento o función en los lenguajes procedimentales y funcionales.

Se procedera a utilizar Jest para realizar las pruebas unitarias. Jest es un marco de prueba de JavaScript creado sobre Jasmine y mantenido por Meta. Fue diseñado y construido por Christoph Nakazawa con un enfoque en la simplicidad y el soporte para grandes aplicaciones web. Funciona con proyectos que utilizan Babel, TypeScript, Node.js, React, Angular, Vue.js y Svelte.

Debido a que en nuestro proyecto utilizamos node.js y React se podra utilizar Jest para la realizacion de pruebas unitarias.

![Jest](imagenes/Jest.jpeg)
