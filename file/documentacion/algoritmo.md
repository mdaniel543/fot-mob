```python
#Crear un predictor de resultados del mundial
#Autor: @byly23

class Nodo:

    def __init__(self, nombre, resultado,golesFavor,golesContra):
        self.nombre=nombre
        self.resultado=resultado
        self.golesFavor=golesFavor
        self.golesContra=golesContra
        self.sucesores=[]

def sucesores(nodo):
"""Devuelve una lista de sucesores de un nodo"""
#2= Ganador, 0=perdedor, 1=empate
if nodo.nombre=="Argentina":
return [Nodo("Australia",2,2,1),Nodo("Polonia",2,2,0),Nodo("Mexico",2,2,0),Nodo("Arabia",0,1,2)]
if nodo.nombre=="Holanda":
return [Nodo("USA",2,3,1),Nodo("Catar",2,2,0),Nodo("Ecuador",1,1,1),Nodo("Senegal",2,2,0)]
if nodo.nombre=="Croacia":
return [Nodo("Japon",2,3,1),Nodo("Belgica",1,0,0),Nodo("Canada",2,4,1),Nodo("Marruecos",1,0,0)]
if nodo.nombre=="Brasil":
return [Nodo("Corea",2,4,1),Nodo("Camerun",0,0,1),Nodo("Suiza",2,1,0),Nodo("Serbia",2,2,0)]
if nodo.nombre=="Marruecos":
return [Nodo("Espana",2,3,0),Nodo("Canada",2,2,1),Nodo("Belgica",2,2,0),Nodo("Croacia",1,0,0)]
if nodo.nombre=="Portugal":
return [Nodo("Suiza",2,6,1),Nodo("Corea",0,1,2),Nodo("Uruguay",2,2,0),Nodo("Ghana",2,3,2)]
if nodo.nombre=="Inglaterra":
return [Nodo("Senegal",2,3,0),Nodo("Gales",2,3,0),Nodo("USA",0,0,0),Nodo("Iran",2,6,2)]
if nodo.nombre=="Francia":
return [Nodo("Polonia",2,3,1),Nodo("Tunez",0,0,1),Nodo("Dinamarca",2,2,1),Nodo("Australia",2,4,1)]

def predict(equipo1,equipo2):
"""Devuelve el resultado de un partido entre dos equipos""" # print(equipo1)
nodo1=Nodo(equipo1,0,0,0)
nodo2=Nodo(equipo2,0,0,0)
sucesores1=sucesores(nodo1)
sucesores2=sucesores(nodo2)
for i in sucesores1:
if i.nombre!=equipo2:
nodo1.golesContra+=i.golesContra
nodo1.golesFavor+=i.golesFavor
nodo1.resultado+=i.resultado
for i in sucesores2:
if i.nombre!=equipo1:
nodo2.golesContra+=i.golesContra
nodo2.golesFavor+=i.golesFavor
nodo2.resultado+=i.resultado
if nodo1.resultado>nodo2.resultado:
cantidadGoles=goles(nodo1.golesFavor+1,nodo1.golesContra-1,nodo2.golesFavor-1,nodo2.golesContra+1,nodo1.nombre,nodo2.nombre)
return [nodo1.nombre,cantidadGoles[1],cantidadGoles[0]]
if nodo1.resultado<nodo2.resultado:
cantidadGoles=goles(nodo1.golesFavor-1,nodo1.golesContra+1,nodo2.golesFavor+1,nodo2.golesContra-1,nodo1.nombre,nodo2.nombre)
return [nodo2.nombre,cantidadGoles[1],cantidadGoles[0]]
if nodo1.resultado==nodo2.resultado:
if nodo1.golesFavor>nodo2.golesFavor:
cantidadGoles=goles(nodo1.golesFavor,nodo1.golesContra,nodo2.golesFavor,nodo2.golesContra,nodo1.nombre,nodo2.nombre)
return [nodo1.nombre,cantidadGoles[1],cantidadGoles[0]]
if nodo1.golesFavor<nodo2.golesFavor:
cantidadGoles=goles(nodo1.golesFavor,nodo1.golesContra,nodo2.golesFavor,nodo2.golesContra,nodo1.nombre,nodo2.nombre)
return [nodo2.nombre,cantidadGoles[1],cantidadGoles[0]]
if nodo1.golesFavor==nodo2.golesFavor:
if nodo1.golesContra<nodo2.golesContra:
cantidadGoles=goles(nodo1.golesFavor,nodo1.golesContra,nodo2.golesFavor,nodo2.golesContra,nodo1.nombre,nodo2.nombre)
return [nodo1.nombre,cantidadGoles[1],cantidadGoles[0]]
if nodo1.golesContra>nodo2.golesContra:
return [nodo2.nombre,cantidadGoles[1],cantidadGoles[0]]
if nodo1.golesContra==nodo2.golesContra:
cantidadGoles=goles(nodo1.golesFavor,nodo1.golesContra,nodo2.golesFavor,nodo2.golesContra,nodo1.nombre,nodo2.nombre)
return ["Penales",cantidadGoles[1],cantidadGoles[0]]

def goles(gol1Favor,gol1Contra,gol2Favor,gol2Contra,equipo1,equipo2):
#Devuelve la probabilidad de que un equipo gane un partido
#en base a los goles que hizo y recibió en los partidos anteriores
#en el mundial
probGolesEqupo1=gol1Favor/(gol1Favor+gol2Contra)
probGolesEquipo2=gol2Favor/(gol2Favor+gol1Contra)
promGolesFavorEqupo1=gol1Favor/4
promGolesContraEqupo1=gol1Contra/4
promGolesFavorEqupo2=gol2Favor/4
promGolesContraEqupo2=gol2Contra/4
cantidadGolesEquipo1=(promGolesFavorEqupo1+promGolesContraEqupo2)*probGolesEqupo1
cantidadGolesEquipo2=(promGolesFavorEqupo2+promGolesContraEqupo1)*probGolesEquipo2
return ["Goles "+equipo1+ ": "+str(cantidadGolesEquipo1),"Goles "+equipo2+ ": "+str(cantidadGolesEquipo2)]

#Ejecutar de uno en uno

# predict("Argentina","Francia")

result = predict("Inglaterra","Marruecos")
Eqp1 = result[1].split()
Eqp2 = result[2].split()
print(result[0],"\n",Eqp1[0]+" "+Eqp1[1],round(float(Eqp1[2])), "\n",Eqp2[0]+" "+Eqp2[1],round(float(Eqp2[2])) )

# predict("Francia","Inglaterra")

# predict("Marruecos","Portugal")
```
