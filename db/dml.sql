#Estado del usuario
INSERT INTO e_u(nombre)
VALUES
('De alta'),
('De baja'),
('Congelada');

#Género de usuario
INSERT INTO genero(nombre)
VALUES
('Femenino'),
('Masculino'),
('Otro');

#Tipos de membresia
INSERT INTO membresia(nombre)
VALUES
('Gratuita'),
('De pago');

#Roles de usuarios
INSERT INTO rol(nombre)
VALUES
('Administrador'),
('Empleado'),
('Cliente');

#Estado de competencias
INSERT INTO e_c(nombre)
VALUES
('En progreso'),
('Finalizada'),
('Programada');

#Tipos de equipos
INSERT INTO t_e(nombre)
VALUES
('Nacional'),
('Club');



#Carga masiva de paises
SELECT * FROM PAIS;
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE PAIS;
SET FOREIGN_KEY_CHECKS = 1;




#TIpo de incidencia
INSERT INTO T_I (nombre)
VALUES 
('Tarjeta'),
('Gol'),
('Auto gol'),
('Gol tiro libre'),
('Gol jugada');

#Tipo de tarjeta
INSERT INTO COLOR (nombre)
VALUES
('Amarilla'),
('Roja');

#Estado del partido
INSERT INTO E_P (nombre)
VALUES
(''),
(''),
(''),
(''),
('');


