const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const request = require("request-promise-native");
const bodyParser = require("body-parser");
// const { request } = require("express");
// Comentario

const app = express();

app.set("port", 80);
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json({ limit: "10mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

//OBTENER REPORTES DE EMPLEADO
app.use("/admin/reporte/empleado/:orden", async (req, res) => {
  try {
    let route = req.originalUrl;
    const empleado = await request("http://mr-pre-2022.tk" + route);
    console.log(req.originalUrl);

    res.json(JSON.parse(empleado));
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

//OBTENER REPORTES DE CLIENTES
app.use("/admin/reporte/cliente", async (req, res) => {
  try {
    let route = req.originalUrl;
    const empleado = await request("http://mr-pre-2022.tk" + route);
    console.log(req.originalUrl);

    res.json(JSON.parse(empleado));
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

//VISUALIZAR INFORMACION DE PARTIDOS
app.use("/cliente/visualizar/partidos", async (req, res) => {
  try {
    let route = req.originalUrl;
    const empleado = await request("http://mr-pre-2022.tk" + route);
    console.log(req.originalUrl);

    res.json(JSON.parse(empleado));
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.listen(app.get("port"), () => {
  console.log("Server on port", app.get("port"));
});
