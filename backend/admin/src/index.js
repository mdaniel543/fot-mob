require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const db = require("./db/conexion");
const cors = require("cors");

const bodyParser = require("body-parser");

const app = express();

const jwt = require("jsonwebtoken");

const admin = require("./routes/admin.routes");
const estadio = require("./routes/estadio.routes");
const competencia = require("./routes/competencia.routes");
const pais = require("./routes/pais.routes");
const reporte = require("./routes/reportes.routes");
const jugador = require("./routes/jugador.routes");
const posicion = require("./routes/posicion.routes");
const bitacora = require("./routes/bitacora.routes");

app.set("port", process.env.PORT || 3004);
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json({ limit: "10mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

app.use("/admin/pais", pais);
app.use(authenticateToken);

app.use("/admin/usuario/", admin, logs_mdlwr);
app.use("/admin/estadio/", estadio);
app.use("/admin/competencia", competencia);
app.use("/admin/reporte",reporte, logs_mdlwr);
app.use("/admin/jugador", jugador);
app.use("/admin/posiciones", posicion);
app.use("/admin/bitacora", bitacora,logs_mdlwr);

app.get("/admin", (req, res) => {
  res.send("Hello! this is Admin Server");
});

app.listen(app.get("port"), () => {
  console.log("Server on port", app.get("port"));
});

// Cambio1235-------------------------
// OtroCambio
function authenticateToken(req, res, next) {
  //console.log("authenticateToken, req.headers", req.headers);
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(400);
  const grupo = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET).num_grupo;
  console.log(grupo)
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(400);
    //if (user.id_rol != 1) return res.sendStatus(400);
    req.grupo = grupo
    req.user = user;
    next();
  });
}

//Middleware
app.use((req, res, next) => {
  try {
    let route = req.originalUrl;
    console.log(route);
  } catch (error) {}
});

/** REGISTRO DE LOGS
 *
 */
async function logs_mdlwr(req, res, next) {
  try {
    let num_grupo = req.num_grupo;
    let route = req.originalUrl;
    let accion = req.accion;
    if (num_grupo == undefined) {
      num_grupo = 0;
    }
    const sql = `CALL crear_bitacora(?,?,?)`;
    const params = [accion, route, num_grupo];
    await db.query(sql, params);
  } catch (error) {
    console.log(error);
  }
}
