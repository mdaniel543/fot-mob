const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const posicion = require("../controllers/posicion.controller");

router.get("/", posicion.get_Posiciones);

module.exports = router;
