const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const jugador = require("../controllers/jugador.controller");

router.get("/", jugador.getJugador);
router.post("/", jugador.crearJugador);
router.put("/:id_jugador", jugador.actualizarJugador);
router.delete("/:id_jugador", jugador.eliminarJugador);
router.get("/partido/:id_partido", jugador.obtener_jugadoresPartido);
router.get("/equipo/:id_equipo", jugador.obtener_jugadoresEquipo);

module.exports = router;
