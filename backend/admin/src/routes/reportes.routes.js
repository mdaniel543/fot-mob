const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const reporte = require("../controllers/reportes.controller");

router.get("/empleado/:orden", reporte.getRep_Empleado);
router.get("/empleado2/:orden/:id_equipo", reporte.getRep_EmpleadoXEquipo);
router.get("/cliente", reporte.getRep_Clientes);
router.get("/cliente2/:id_equipo", reporte.getRep_Clientes2);

module.exports = router;
