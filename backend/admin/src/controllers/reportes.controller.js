require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

exports.getRep_Empleado = async (req, res,next) => {
  try {
    const { orden } = req.params;
    if (!orden) {
      return res.status(400).json({ message: "Faltan datos" });
    }
    const sql = `CALL obtener_reporte_empleados(?)`;
    const result = await db.query(sql, [orden]);
    res.status(200).json({ message: "Reporte Empleados", data: result[0] });
    req.accion = `se obtuvo el reporte de empleados`
    req.num_grupo = req.grupo;
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener Reporte Empleados" });
  }
};

exports.getRep_EmpleadoXEquipo = async (req, res,next) => {
  try {
    const { orden, id_equipo } = req.params;
    if (!orden || !id_equipo) {
      return res.status(400).json({ message: "Faltan datos" });
    }
    const sql = `CALL obtener_reporte_empleados_equipo(?,?)`;
    const result = await db.query(sql, [orden, id_equipo]);
    res
      .status(200)
      .json({ message: "Reporte Empleados X Equipo", data: result[0] });
      req.accion = `se obtuvo el reporte de empleados por equipo`
      req.num_grupo = req.grupo;
      next();
  } catch (error) {
    console.log(error);
    res
      .status(400)
      .json({ message: "Error al obtener Reporte Empleados X Equipo" });
  }
};

exports.getRep_Clientes = async (req, res,next) => {
  try {
    const sql = `CALL obtener_reporte_clientes()`;
    const result = await db.query(sql);
    // console.log(result);
    res.status(200).json({data: result[0]});
    req.accion = `se obtuvo el reporte de clientes`
    req.num_grupo = req.grupo;
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener Reporte Clientes" });
  }
};

exports.getRep_Clientes2 = async (req, res,next) => {
  try {
    const { id_equipo } = req.params;
    if (!id_equipo) {
      return res.status(400).json({ message: "Faltan datos" });
    }
    const sql = `CALL obtener_usuarios_suscritos_equipo(?)`;
    const result = await db.query(sql, [id_equipo]);
    res.status(200).json(result[0]);
    req.accion = `se obtuvo el reporte de clientes 2`
    req.num_grupo = req.grupo;
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener Reporte Clientes2" });
  }
};
