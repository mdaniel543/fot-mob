require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

exports.getJugador = async (req, res) => {
  try {
    const sql = `CALL obtener_jugadores()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener jugadores" });
  }
};

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} fecha_nac date,
 * @param {*} id_pais INT,
 * @param {*} id_posicion INT
 */
exports.crearJugador = async (req, res) => {
  const { nombre, fecha_nac, id_pais, id_posicion } = req.body;

  if (!nombre || !fecha_nac || !id_pais || !id_posicion) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL crear_jugador(?,?,?,?)`;
    const params = [nombre, fecha_nac, id_pais, id_posicion];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al crear juagdor" });
  }
};

/**
 * @param {*} id_jugador INT,
 * @param {*} nombre VARCHAR(100),
 * @param {*} id_posicion INT
 */
exports.actualizarJugador = async (req, res) => {
  const { id_jugador } = req.params;
  const { nombre, id_posicion } = req.body;

  if (!nombre || !id_jugador || !id_posicion) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL actualizar_jugador(?,?,?)`;
    const params = [id_jugador, nombre, id_posicion];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al actualizar jugador" });
  }
};

/**
 * @param {*} id_jugador INT
 */
exports.eliminarJugador = async (req, res) => {
  const { id_jugador } = req.params;

  if (!id_jugador) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL eliminar_jugador(?)`;
    const params = [id_jugador];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al eliminar jugador" });
  }
};

/**
 * @param {*} id_partido INT
 */
exports.obtener_jugadoresPartido = async (req, res) => {
  const { id_partido } = req.params;

  if (!id_partido) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL obtener_jugadores_partido(?)`;
    const params = [id_partido];
    const result = await db.query(sql, params);
    res.status(200).json(result[0][0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al Obtener jugadores del partido" });
  }
};

/**
 * @param {*} id_equipo INT
 */
exports.obtener_jugadoresEquipo = async (req, res) => {
  const { id_equipo } = req.params;

  if (!id_equipo) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL obtener_jugadores_equipo(?)`;
    const params = [id_equipo];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al Obtener jugadores del equipo" });
  }
};
