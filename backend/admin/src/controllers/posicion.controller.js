require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

exports.get_Posiciones = async (req, res) => {
  try {
    const sql = `CALL obtener_posiciones()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener posiciones" });
  }
};
