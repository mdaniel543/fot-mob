require('dotenv/config')
const bcrypt = require('bcrypt')
const db = require('../db/conexion')
const jwt = require('jsonwebtoken')
const moment = require('moment')

exports.obtenerCompetencia = async (req, res) => {
  try {
    const sql = `CALL obtener_competencias()`
    const result = await db.query(sql)
    res.status(200).json(result[0])
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al obtener competencias' })
  }
}

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} fecha VARCHAR(100),
 * @param {*} id_pais INT,
 * @param {*} estado INT
 */
exports.crearCompetencia = async (req, res) => {
  const { nombre, fecha, id_pais, estado } = req.body

  if (!nombre || !fecha || !id_pais || !estado) {
    return res.status(400).json({ message: 'Faltan datos' })
  }
  // let fechas = moment(fecha_fun).format("DD-MM-YYYY");
  try {
    const sql = `CALL crear_competencia(?,?,?,?)`
    const params = [nombre, fecha, id_pais, estado]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al crear competencia' })
  }
}

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} estado INT,
 * @param {*} id_competencia INT
 */
exports.actualizarCompetencia = async (req, res) => {
  const { nombre, estado, id_competencia } = req.body

  if (!nombre || !estado || !id_competencia) {
    console.log(nombre, estado, id_competencia)
    return res.status(400).json({ message: 'Faltan datos' })
  }

  try {
    const sql = `CALL actualizar_competencia(?,?,?)`
    const params = [id_competencia, nombre, estado]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al actualizar competencia' })
  }
}

/**
 * @param {*} id_competencia INT
 */
exports.eliminarCompetencia = async (req, res) => {
  const { id_competencia } = req.params

  if (!id_competencia) {
    return res.status(400).json({ message: 'Faltan datos' })
  }

  try {
    const sql = `CALL eliminar_competencia(?)`
    const params = [id_competencia]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al eliminar competencia' })
  }
}
