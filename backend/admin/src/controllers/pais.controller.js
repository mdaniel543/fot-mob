require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");



/**
 * obtener_paises
 */
exports.get = async (req, res) => {
    try {
        const sql = `CALL obtener_paises()`;
        const result = await db.query(sql);
        res.status(200).json(result[0]);
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al obtener paises" });
    }
}


