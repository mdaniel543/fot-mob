const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA PAIS /", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "admin@gmail.com",
        password: "admin",
        num_grupo: 8
      })
      .end((err, response) => {
        console.log(response.body);
        token = response.body.token; // save the token!
        done();
      });
  });
  test("Este test verificara obtener pais", async () => {
    const result = await req("http://localhost:3004")
      .get("/admin/pais")
      .set("Authorization", `Bearer ${token}`)
      .send();
    expect(result.statusCode).toBe(200);
  }
  );
});
