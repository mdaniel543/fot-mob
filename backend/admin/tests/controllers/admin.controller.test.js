const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA PUT /", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "admin@gmail.com",
        password:"admin",
        num_grupo: 8
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.token; // save the token!
        done();
      });
  });
  test("Este test verificara el cambio de estado de un usuario",async () => {
    const result = await req("http://localhost:3004")
      .put("/admin/usuario/estado")
      .set("Authorization", `Bearer ${token}`)
      .send({
        "id_usuario": 2,
        "id_estado": 1
      })
       expect(result.statusCode).toBe(200);
  });
});

////