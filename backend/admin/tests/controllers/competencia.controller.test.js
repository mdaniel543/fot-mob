const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA ACTUALIZAR COMPETENCIA /", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "admin@gmail.com",
        password:"admin",
        num_grupo: 8
      })
      .end((err, response) => {
        
        token = response.body.token; // save the token!
        done();
      });
  });
  test("Este test verificara el cambio de estado de un usuario",async () => {
    const result = await req("http://localhost:3004")
      .put("/admin/competencia/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        "nombre": "La Real Competencia1",
        "estado": 1,
        "id_competencia": 2
      })
       
      expect(result.statusCode).toBe(200);
  });
});

describe("PRUEBA UNITARIA OBTENER COMPETENCIA /", () => {
      let token = "";
      beforeAll((done) => {
        req("http://localhost:3005")
          .post("/api/sesion/iniciar")
          .send({
            email: "admin@gmail.com",
            password:"admin",
            num_grupo: 8
          })
          .end((err, response) => {
            //console.log(response.body);
            token = response.body.token; // save the token!
            done();
          });
      });
      test("Este test verificara obtener competencias",async () => {
        const result = await req("http://localhost:3004")
          .get("/admin/competencia/")
          .set("Authorization", `Bearer ${token}`)
          .send()
          expect(result.statusCode).toBe(200);
      });
    });