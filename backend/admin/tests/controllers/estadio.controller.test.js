const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA ESTADIO /", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "admin@gmail.com",
        password:"admin",
        num_grupo: 8
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.token; // save the token!
        done();
      });
  });
  test("Este test verificara que no se pueda editar un estadio con el mismo nombre", async () => {
    const result = await req("http://localhost:3004")
      .put("/admin/estadio/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        nombre: "Arco del triunfo 2",
        capacidad: 1,
        id_estadio: 2,
      });
    expect(result.statusCode).toBe(200);
  });
});

describe("PRUEBA UNITARIA OBTENER ESTADIO /", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "admin@gmail.com",
        password:"admin",
        num_grupo: 8
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.token;// save the token!
        done();
      });
  });
  test("Este test verificara obtener estadio", async () => {
    const result = await req("http://localhost:3004")
      .get("/admin/estadio/")
      .set("Authorization", `Bearer ${token}`)
      .send();
    expect(result.statusCode).toBe(200);
  });
});
