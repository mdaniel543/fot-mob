const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA ACTUALIZAR CLIENTE /", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "bbowserl@go.com",
        password:"DsHkCbO7ojB",
        num_grupo:8
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.token; // save the token!
        done();
      });
  });
  test("Este test verificara la actualizacion de un cliente", async () => {
    const result = await req("http://localhost:3002")
      .put("/cliente/actualizar/2")
      .set("Authorization", `Bearer ${token}`)
      .send({
        nombre: "Juan",
        apellido: "Perez",
        telefono: "12345678",
        direccion: "Calle 123",
        id_genero: 1,
        id_pais: 1,
      });
    expect(result.statusCode).toBe(200);
  });
});


////