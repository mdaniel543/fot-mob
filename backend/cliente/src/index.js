require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const bodyParser = require("body-parser");

const app = express();

const jwt = require("jsonwebtoken");

const cliente = require("./routes/cliente.routes");
const membresia = require("./routes/membresia.routes");
const suscripcion = require("./routes/suscripcion.routes");
const history = require("./routes/history.routes");
const estadistica = require("./routes/estadistica.routes");
const quinela = require("./routes/quinela.routes");

app.set("port", process.env.PORT || 3002);
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//app.use(authenticateToken);

app.use("/cliente", cliente);
app.use("/cliente/membresia", membresia);
app.use("/cliente/visualizar", history);
app.use("/cliente/suscripcion", suscripcion);
app.use("/cliente/estadistica", estadistica);
app.use("/cliente/quiniela", quinela);

app.get("/", (req, res) => {
  res.send("Hello! this is Admin Server");
});

app.listen(app.get("port"), () => {
  console.log("Server on port", app.get("port"));
});

function authenticateToken(req, res, next) {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(400);
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(400);
    if (user.id_rol != 3) return res.sendStatus(400);
    req.user = user;
    next();
  });
}

// Comentario
