require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

/**
 * @param {*} id_usuarioIN INT,
 * @param {*} nombreIN VARCHAR(100),
 * @param {*} apellidoIN VARCHAR(100),
 * @param {*} telefonoIN VARCHAR(12),
 * @param {*} direccionIN VARCHAR(200)
 * actualizar_informacion_usuario
 */

exports.actualizar = async (req, res) => {
  const { id_usuario } = req.params;
  const { nombre, apellido, telefono, direccion, id_genero, id_pais } =
    req.body;
  if (!id_usuario || !nombre || !apellido || !telefono || !direccion || !id_genero || !id_pais) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL actualizar_informacion_usuario(?,?,?,?,?,?,?)`;
    const params = [
      id_usuario,
      nombre,
      apellido,
      telefono,
      direccion,
      id_genero,
      id_pais,
    ];
    const result = await db.query(sql, params);
    res.status(200).json({ message: "actualizado con exito" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al actualizar usuario" });
  }
};

/**
 * obtener_informacion_partidos
 */
exports.visualizar_partidos = async (req, res) => {
  try {
    const sql = `CALL obtener_informacion_partidos()`;
    const result = await db.query(sql);
    res
      .status(200)
      .json({ message: "partidos obtenidos con exito", data: result[0] });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener partidos" });
  }
};
//---
