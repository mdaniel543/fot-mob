require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

/**
 * @param {*} id_usuario INT
 * obtener_historial_membresias
 */
exports.historial = async (req, res) => {
    const { id_usuario } = req.params;;
    if (!id_usuario) {
        return res.status(400).json({ message: "Faltan datos" });
    }
    try {
        const sql = `CALL obtener_historial_membresias(?)`;
        const params = [id_usuario];
        const result = await db.query(sql, params);
        res.status(200).json({data: result[0]});
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al obtener historial de membresias" });
    }
}

/**
 * @param {*} id_usuario INT,
 * @param {*} duracion INT
*/
exports.adquirir = async (req, res) => {
    const { id_usuario, meses } = req.body;
    console.log(req.body)
    if (!id_usuario || !meses) {
        return res.status(400).json({ message: "Faltan datos" });
    }
    try {
        const sql = `CALL adquirir_membresia(?,?)`;
        const params = [id_usuario, meses];
        const result = await db.query(sql, params);
        res.status(200).json({ message: result[0][0].message });
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al adquirir membresia" });
    }
}

/**
 * @param {*} id_usuario INT
 * ver_membresia
 */
exports.get = async (req, res) => {
    const { id_usuario } = req.params;
    if (!id_usuario) {
        return res.status(400).json({ message: "Faltan datos" });
    }
    try {
        const sql = `CALL ver_membresia(?)`;
        const params = [id_usuario];
        const result = await db.query(sql, params);
        res.status(200).json(result[0][0]);
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al obtener membresia" });
    }
}

/**
 * @param {*} id_usuarioIN INT
 * dar_baja_membresia
 */
exports.delete = async (req, res) => {
    const { id_usuario } = req.body;
    if (!id_usuario) {
        return res.status(400).json({ message: "Faltan datos" });
    }
    try {
        const sql = `CALL dar_baja_membresia(?)`;
        const params = [id_usuario];
        const result = await db.query(sql, params);
        res.status(200).json({ message: result[0][0].message });
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al dar de baja membresia" });
    }
}