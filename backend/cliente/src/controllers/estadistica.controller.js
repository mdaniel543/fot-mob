require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

exports.consultar_Jugadores1 = async (req, res) => {
  try {
    const sql = `CALL obtener_reporte_jugadores_1()`;
    const result = await db.query(sql);
    res.status(200).json({
      message: "CONSULTAR DATOS ESTADISTICOS JUGADORES 1",
      data: result[0],
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

exports.consultar_Jugadores2 = async (req, res) => {
  try {
    const sql = `CALL obtener_reporte_jugadores_2()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

/**
 * @param {*} id_deportista INT,
 * @param {*} tipo_deportista INT
 */
exports.consultar_Jugadores3 = async (req, res) => {
  const { id_deportista, tipo_deportista } = req.params;
  console.log(req.body);
  if (!id_deportista || !tipo_deportista) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL historial_equipos_jugador_tecnico(?,?)`;
    const params = [id_deportista, tipo_deportista];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

exports.consultar_tecnico = async (req, res) => {
  try {
    const sql = `CALL obtener_reporte_tecnicos()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

exports.consultar_partido = async (req, res) => {
  try {
    const sql = `CALL obtener_reporte_partidos()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

exports.consultar_estadio = async (req, res) => {
  try {
    const sql = `CALL obtener_reporte_estadios()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

exports.consultar_equipo = async (req, res) => {
  try {
    const sql = `CALL obtener_reporte_equipos()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

/**
 * @param {*} id_competencia INT
 */
exports.consultar_competencia1 = async (req, res) => {
  const { id_competencia } = req.params;
  console.log(req.body);
  if (!id_competencia) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL obtener_reporte_competencia1(?)`;
    const params = [id_competencia];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};

/**
 * @param {*} id_equipo INT
 */
exports.consultar_competencia2 = async (req, res) => {
  const { id_equipo } = req.params;
  // console.log(req.body);
  if (!id_equipo) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL obtener_reporte_competencia2(?)`;
    const params = [id_equipo];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al consultar datos" });
  }
};
