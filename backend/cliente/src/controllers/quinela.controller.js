require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

/**
 * @param {*} id_partido INT,
 * @param {*} id_usuario INT,
 * @param {*} gol_loc INT,
 * @param {*} gol_vis INT
 */
exports.crear_Quinelas = async (req, res) => {
  const { id_partido, id_usuario, gol_loc, gol_vis } = req.body;
  if (!id_partido || !id_usuario || !gol_loc || !gol_vis) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL crear_quiniela(?,?,?,?)`;
    const params = [id_partido, id_usuario, gol_loc, gol_vis];
    const result = await db.query(sql, params);
    res.status(200).json(result[0][0].message);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al crear quinielas" });
  }
};

/**
 * @param {*} id_quiniela INT,
 * @param {*} gol_loc INT,
 * @param {*} gol_vis INT
 */
exports.actualizar_Quinelas = async (req, res) => {
  const { id_quiniela, gol_loc, gol_vis } = req.body;
  if (!id_quiniela || !gol_loc || !gol_vis) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL actualizar_quiniela(?,?,?)`;
    const params = [id_quiniela, gol_loc, gol_vis];
    const result = await db.query(sql, params);
    res.status(200).json(result[0][0].message);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al actualizar quinielas" });
  }
};

/**
 * @param {*} id_usuario INT
 */
exports.obtener_Quinela = async (req, res) => {
  const { id_usuario } = req.params;
  if (!id_usuario) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL obtener_quinielas(?)`;
    const params = [id_usuario];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener quinielas" });
  }
};
