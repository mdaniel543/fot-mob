const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const suscripcion = require("../controllers/suscripcion.controller");

router.get("/equipo/:id_usuario", suscripcion.get_EquipoNoSubscrito);

router.post("/", suscripcion.SubscripcionEquipo);

router.get("/noticias/:id_usuario", suscripcion.verNoticia_Subscripcion);

module.exports = router;
