const express = require("express");
const router = express.Router();

const membresia = require("../controllers/membresia.controller");

router.get("/membresias/:id_usuario", membresia.historial);



module.exports = router;

