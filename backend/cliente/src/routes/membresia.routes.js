const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const membresia = require("../controllers/membresia.controller");


router.get("/:id_usuario",membresia.get);

router.post("/activar", membresia.adquirir);

router.put("/baja", membresia.delete);


module.exports = router;
