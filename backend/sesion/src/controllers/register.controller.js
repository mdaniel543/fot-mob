require("dotenv/config");
const aws = require("aws-sdk");
const { awsKeyS3 } = require("../credentials/credentials");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");
const { SHA256 } = require("crypto-js");

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} clave_acceso VARCHAR(256),
 * @param {*} apellido VARCHAR(100),
 * @param {*} correo VARCHAR(100),
 * @param {*} telefono VARCHAR(10),
 * @param {*} fotografia VARCHAR(200),
 * @param {*} fecha_nac VARCHAR(12),
 * @param {*} direccion VARCHAR(200),
 * @param {*} id_rol INT, Can be 1, 2 or 3 (Admin, empleado, Cliente)
 * @param {*} id_genero INT,
 * @param {*} id_pais INT
 */
exports.register = async (req, res,next) => {
  const {
    correo,
    clave_acceso,
    nombre,
    apellido,
    telefono,
    fotografia,
    fecha_nac,
    direccion,
    id_rol,
    id_genero,
    id_pais
  } = req.body;

  if (
    !correo ||
    !clave_acceso ||
    !nombre ||
    !apellido ||
    !telefono ||
    !fotografia ||
    !fecha_nac ||
    !direccion ||
    !id_rol ||
    !id_genero ||
    !id_pais
  ) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  const hashedPassword = SHA256(clave_acceso).toString();

  try {
    const pathPhoto = await uploadFile(fotografia, "jpg", correo);

    if (pathPhoto.respuesta == "error") {
      return res.status(400).json({ message: "Error al registrar" });
    }

    const sql = `CALL crear_usuario(?,?,?,?,?,?,?,?,?,?,?)`;
    const params = [
      correo,
      hashedPassword,
      nombre,
      apellido,
      telefono,
      pathPhoto,
      fecha_nac,
      direccion,
      id_rol,
      id_genero,
      id_pais,
    ];

    const result = await db.query(sql, params);

    res.status(200).json({ message: result[0][0].message });
    req.accion = "Se registro un nuevo usuario"
    req.num_grupo = req.grupo;
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al registrar" });
  }
};

const uploadFile = async (image, imageExt, correo) => {
  try {
    const nombre = correo.split(".");
    const path = `${nombre[0]}_foto_perfil.${imageExt}`;
    var fullPath = path.replace("@", "%40");
    fullPath = "https://tallerpi.s3.amazonaws.com/" + fullPath;
    let buff = new Buffer.from(image, "base64");

    aws.config.update({
      region: awsKeyS3.region,
      accessKeyId: awsKeyS3.accessKeyId,
      secretAccessKey: awsKeyS3.secretAccessKey,
    });

    var s3 = new aws.S3();
    const params = {
      Bucket: "tallerpi",
      Key: path,
      Body: buff,
      ContentType: "image",
    };

    const result = s3.putObject(params).promise();
    return fullPath;
  } catch (error) {
    console.log(error);
    return "error";
  }
};
