require("dotenv/config");
const aws = require("aws-sdk");
const { awsKeyS3 } = require("../credentials/credentials");
const { SHA256 } = require("crypto-js");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const { v4: uuidv4 } = require("uuid");

/**
 *
 * @param {*} email
 * @param {*} password
 */

exports.logIn = async (req, res,next) => {
  //console.log(req.body);
  const { email, correo, password,num_grupo} = req.body;
  const usuario = email || correo;
  console.log(req.body);
  try {
    const sql = `CALL login(?)`;
    const params = [usuario];
    const result = await db.query(sql, params);
    
    const user = {
      ...result[0][0],
      clave_acceso: undefined,
      num_grupo : num_grupo
    };
    console.log(user);
    if (result[0][0].id_estado == 3) {
      return res.status(400).json({ message: "Cuenta no activada" });
    } else if (result[0][0].id_estado == 2) {
      return res.status(400).json({ message: "Cuenta no existe" });
    }
    const hashedPassword = SHA256(password).toString();

    const temp = await db.query(
      `CALL obtener_pass_temporal(${result[0][0].id_usuario})`
    );

    if (temp[0][0].respuesta != null) {
      if (hashedPassword == temp[0][0].respuesta) {
        const token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
          expiresIn: "604800s",
        });
        console.log("Login successful");
        return res.status(200).json({
          token: token,
          message: "Inicio de sesion exitoso",
          tipo: 2,
        });
      }
    }
    if (hashedPassword == result[0][0].clave_acceso) {
      const token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
        expiresIn: "604800s",
      });
      console.log("Login successful");
      res.status(200).json({
        token: token,
        message: "Inicio de sesion exitoso",
        tipo: 1,
      });
    } else {
      console.log("Login failed");
      res.status(400).json({ message: "Error en Credenciales" });
    }
    req.accion = `Inicio de sesion usuario id ${result[0][0].id_usuario}`;
    req.num_grupo = num_grupo;
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "error al iniciar sesion" });
  }
};

/**
 *  nombre: string
 *  clave_acceso: string
 *  apellido: string
 *  correo: string
 *  telefono: string
 *  fotografia: string << BASE 64 >>
 *  fecha_nac: string <YYYY-MM-DD>
 *  direccion: string
 *  id_rol: int <<  3: Cliente>>
 *  id_genero: int << 1: F,  2: M >>
 *  id_pais: int
 */

exports.register = async (req, res,next) => {
  const {
    nombre,
    clave_acceso,
    apellido,
    correo,
    telefono,
    fotografia,
    fecha_nac,
    direccion,
    id_genero,
    id_pais,
  } = req.body;

  if (
    !nombre ||
    !clave_acceso ||
    !apellido ||
    !correo ||
    !telefono ||
    !fotografia ||
    !fecha_nac ||
    !direccion ||
    !id_genero ||
    !id_pais
  ) {
    return res.status(400).json({ message: "Error al registrar" });
  }

  const hashedPassword = SHA256(clave_acceso).toString();
  //actualizar_estado_usuario(id_usuario, 1);
  try {
    const pathPhoto = await uploadFile(fotografia, "jpg", correo);

    if (pathPhoto.respuesta == "error") {
      return res.status(400).json({ message: "Error al registrar" });
    }

    const sql = `CALL crear_usuario(?,?,?,?,?,?,?,?,?,?,?)`;
    const params = [
      correo,
      hashedPassword,
      nombre,
      apellido,
      telefono,
      pathPhoto,
      fecha_nac,
      direccion,
      3,
      id_genero,
      id_pais,
    ];

    //console.log("++++++++++++++++++++++++++++++++++++");
    const result = await db.query(sql, params);

    const sql2 = `CALL obtener_datos_usuario(?)`;

    const params2 = [correo];

    const usuario = await db.query(sql2, params2);

    const id_user = usuario[0][0].id_usuario;
    console.log(id_user);
    const token = jwt.sign({ id_user }, process.env.ACCESS_TOKEN_SECRET);
    console.log(token);
    sendEmail(
      correo,
      "Activar Cuenta",
      `Para poder activar su cuenta entre al siguiente enlace: \n\n <a href="http://service-esb-g8.tk/api/sesion/confirmar/${token}">Link de Activacion</a>`
    );
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al registrar" });
  }
};

const uploadFile = async (image, imageExt, correo) => {
  try {
    const nombre = correo.split(".");
    const path = `${nombre[0]}_foto_perfil.${imageExt}`;
    var fullPath = path.replace("@", "%40");
    fullPath = "https://tallerpi.s3.amazonaws.com/" + fullPath;
    let buff = new Buffer.from(image, "base64");

    aws.config.update({
      region: awsKeyS3.region,
      accessKeyId: awsKeyS3.accessKeyId,
      secretAccessKey: awsKeyS3.secretAccessKey,
    });

    var s3 = new aws.S3();
    const params = {
      Bucket: "tallerpi",
      Key: path,
      Body: buff,
      ContentType: "image",
    };

    const result = s3.putObject(params).promise();
    return fullPath;
  } catch (error) {
    console.log(error);
    return "error";
  }
};

exports.activeAccount = async (req, res) => {
  const { token } = req.params;
  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    console.log(decoded.id_user);
    const sql = `CALL actualizar_estado_usuario(?,?)`;
    const params = [decoded.id_user, 1];
    await db.query(sql, params);
    res.status(200).json({ message: "Cuenta activada" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al activar cuenta" });
  }
};

exports.forgotPassword = async (req, res) => {
  const { correo } = req.body;
  const sql = `CALL login(?)`;
  const params = [correo];
  const result = await db.query(sql, params);
  const pswrd_recovery = uuidv4().split("-")[4];
  console.log(pswrd_recovery);

  try {
    const sql2 = `CALL registrar_pass_temporal(?,?)`;
    const params2 = [
      result[0][0].id_usuario,
      SHA256(pswrd_recovery).toString(),
    ];
    await db.query(sql2, params2);
    const text = `Usted ha solicitado recuperar contraseña: 
    \n\n Su usuario es : ${correo}
    \n\n Su nueva contraseña es: ${pswrd_recovery}`;
    await sendEmail(correo, "Recuperar contraseña", text);
    res.status(200).json({ message: "Email enviado" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al recuperar contraseña" });
  }
};

/**
 * @body id_usuario : int
 * @body password : string
 * actualizar_password
 */
exports.update_password = async (req, res) => {
  try {
    const { id_usuario, password } = req.body;
    if (!id_usuario || !password) {
      res.status(400).json({ message: "Faltan datos" });
    }
    const sql = `CALL actualizar_password(?,?)`;
    const hashedPassword = SHA256(password).toString();
    const params = [id_usuario, hashedPassword];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al actualizar contraseña" });
  }
};

async function sendEmail(email, subject, text) {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "jjcccou@gmail.com",
      pass: "uraogszndirevxfu",
    },
  });

  let info = {
    from: `Patea Bonito <pateabonito@info>`,
    to: email,
    subject: subject,
    html: `<body>${text}</body>`,
  };

  transporter.sendMail(info, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Email sent");
    }
  });
}
