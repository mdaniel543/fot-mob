require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const db = require("./db/conexion");

const bodyParser = require("body-parser");

const app = express();

const jwt = require("jsonwebtoken");

const sesion = require("./routes/sesion.routes");
const register = require("./routes/register.routes");
const esb = require("./routes/esb.routes");

app.set("port", process.env.PORT || 3005);
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json({ limit: "10mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

app.use("/api/sesion", sesion, logs_mdlwr);
app.use("/api/admin",authenticateToken,register, logs_mdlwr);
app.use("/api/esb", esb);

app.get("/", (req, res) => {
  res.send("Hello! this is Sesion Server");
});

app.listen(app.get("port"), () => {
  console.log("Server on port", app.get("port"));
});

/** REGISTRO DE LOGS
 *
 */
async function logs_mdlwr(req, res, next) {
  try {
    let num_grupo = req.num_grupo;
    let route = req.originalUrl;
    let accion = req.accion;
    const sql = `CALL crear_bitacora(?,?,?)`;
    console.log(accion, route, num_grupo);
    const params = [accion, route, num_grupo];
    await db.query(sql, params);
  } catch (error) {
    console.log(error);
  }
}

function authenticateToken(req, res, next) {
  //console.log("authenticateToken, req.headers", req.headers);
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(400);
  const grupo = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET).num_grupo;
  console.log(grupo)
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(400);
    //if (user.id_rol != 1) return res.sendStatus(400);
    req.grupo = grupo
    req.user = user;
    next();
  });
}
