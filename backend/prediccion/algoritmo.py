
class Prediccion:
    def __init__(self,Equipo1,Equpo2,ResultadoEquipo1,ResultadoEquipo2,Ganador):
        self.equipo1=Equipo1
        self.equipo2=Equpo2
        self.resultadoEquipo1=ResultadoEquipo1
        self.resultadoEquipo2=ResultadoEquipo2
        self.ganador=Ganador

def predict(equipo1,equipo2):
    """Devuelve el resultado de un partido entre dos equipos"""
    # print(equipo1)   
    
    cantidadGoles=goles(equipo1.golesFavor,equipo1.golesContra,equipo2.golesFavor,equipo2.golesContra, equipo1.equiposCombatidos,equipo2.equiposCombatidos)
    if equipo1.resultado>equipo2.resultado:
        EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0])+1,round(cantidadGoles[1]),equipo1.nombre)
        return EquipoGanador
    if equipo1.resultado<equipo2.resultado:
        EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0]),round(cantidadGoles[1])+1,equipo2.nombre)
        return EquipoGanador
    if equipo1.resultado==equipo2.resultado:
        if cantidadGoles[0]>cantidadGoles[1]:
            EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0])+1,round(cantidadGoles[1]),equipo1.nombre)
            return EquipoGanador
        if cantidadGoles[0]<cantidadGoles[1]:
            EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0]),round(cantidadGoles[1])+1,equipo2.nombre)
            return EquipoGanador
        # if equipo1.golesFavor>equipo2.golesFavor:
        #     EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0])+1,round(cantidadGoles[1]),equipo1.nombre)
        #     return EquipoGanador
        # if equipo1.golesFavor<equipo2.golesFavor:
        #     EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0]),round(cantidadGoles[1])+1,equipo2.nombre)
        #     return EquipoGanador
        # if equipo1.golesFavor==equipo2.golesFavor:
        #     if equipo1.golesContra<equipo2.golesContra:    
        #         EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0])+1,round(cantidadGoles[1]),equipo1.nombre)
        #         return EquipoGanador
        #     if equipo1.golesContra>equipo2.golesContra:
        #         EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0]),round(cantidadGoles[1])+1,equipo2.nombre)
        #         return EquipoGanador
        #     if equipo1.golesContra==equipo2.golesContra:    
        #         EquipoGanador= Prediccion(equipo1.nombre,equipo2.nombre,round(cantidadGoles[0]),round(cantidadGoles[1]),"Empate")
        #         return EquipoGanador

def goles(gol1Favor,gol1Contra,gol2Favor,gol2Contra,partidosCombatidos1,partidosCombatidos2):
    #Devuelve la probabilidad de que un equipo gane un partido
    #en base a los goles que hizo y recibió en los partidos anteriores
    #en el mundial
    
    probGolesEqupo1=(gol1Favor/(gol1Favor+gol2Contra)) 
    probGolesEquipo2=(gol2Favor/(gol2Favor+gol1Contra)) 
    promGolesFavorEqupo1=(gol1Favor/partidosCombatidos1) if partidosCombatidos1!=0 else 0
    promGolesContraEqupo1=(gol1Contra/partidosCombatidos1) if partidosCombatidos1!=0 else 0
    promGolesFavorEqupo2=(gol2Favor/partidosCombatidos2) if partidosCombatidos2!=0 else 0
    promGolesContraEqupo2=(gol2Contra/partidosCombatidos2) if partidosCombatidos2!=0 else 0
    cantidadGolesEquipo1=(promGolesFavorEqupo1+promGolesContraEqupo2)*probGolesEqupo1
    cantidadGolesEquipo2=(promGolesFavorEqupo2+promGolesContraEqupo1)*probGolesEquipo2
    return [cantidadGolesEquipo1,cantidadGolesEquipo2]

#Ejecutar de uno en uno
# predict("Argentina","Francia")
# result = predict("Inglaterra","Marruecos")
# Eqp1 = result[1].split()
# Eqp2 = result[2].split()
# print(result[0],"\n",Eqp1[0]+" "+Eqp1[1],round(float(Eqp1[2])), "\n",Eqp2[0]+" "+Eqp2[1],round(float(Eqp2[2])) )

# predict("Francia","Inglaterra")
# predict("Marruecos","Portugal")