const req = require("supertest");
const { SHA256 } = require("crypto-js");

/**
 * /empleado/equipo/
 * 
 */

describe("PRUEBA UNITARIA OBTENER EQUIPOS /", () => {
    let token = "";
    beforeAll((done) => {
      req("http://localhost:3005")
        .post("/api/sesion/iniciar")
        .send({
          email: "empleado1@gmail.com",
          password:"empleado1",
        })
        .end((err, response) => {
          //console.log(response.body);
          token = response.body.token; // save the token!
          done();
        });
    });
    test("Este test verificara la obtencion de un equipo", async () => {
        const result = await req("http://localhost:3003")
        .get("/empleado/equipo/")
        .set("Authorization", `Bearer ${token}`)
        .send();
        expect(typeof result.body[0].id_equipo).toBe("number");
        expect(typeof result.body[0].nombre).toBe("string");
        expect(typeof result.body[0].fecha_fun).toBe("string");
        expect(typeof result.body[0].pais).toBe("string");
        expect(typeof result.body[0].tipo).toBe("string");
        expect(result.statusCode).toBe(200);
    });
});

describe("PRUEBA UNITARIA ACTUALIZAR EQUIPO /", () => {
    let token = "";
    beforeAll((done) => {
      req("http://localhost:3005")
        .post("/api/sesion/iniciar")
        .send({
          email: "empleado1@gmail.com",
          password:"empleado1",
        })
        .end((err, response) => {
          console.log(response.body);
          token = response.body.token; // save the token!
          done();
        });
    });
    test("Este test verificara la actualizacion de un equipo", async () => {
        const result = await req("http://localhost:3003")
        .put("/empleado/equipo/")
        .set("Authorization", `Bearer ${token}`)
        .send({
            "id_equipo": 1,
            "nombre": "Deportivo la coruña"
        })
        expect(result.body.message).toBe('Equipo actualizado con éxito.')
        expect(result.statusCode).toBe(200);
    })
});
