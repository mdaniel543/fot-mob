const req = require("supertest");
const { SHA256 } = require("crypto-js");

/**
 * /empleado/noticias/
 */

describe("PRUEBA UNITARIA OBTENER NOTICIAS /", () => {
    let token = "";
    beforeAll((done) => {
      req("http://localhost:3005")
        .post("/api/sesion/iniciar")
        .send({
          email: "empleado1@gmail.com",
          password:"empleado1",
        })
        .end((err, response) => {
          //console.log(response.body);
          token = response.body.token; // save the token!
          done();
        });
    });
    test("Este test verificara la obtencion de una noticia", async () => {
        const result = await req("http://localhost:3003")
        .get("/empleado/noticias/")
        .set("Authorization", `Bearer ${token}`)
        .send();
        expect(typeof result.body).toBe("object");
        expect(result.statusCode).toBe(200);
    })
})