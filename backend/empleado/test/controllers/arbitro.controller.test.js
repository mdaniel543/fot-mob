const req = require("supertest");
const { SHA256 } = require("crypto-js");

/**
 * /empleado/arbitro/
 * 
 */

describe("PRUEBA UNITARIA OBTENER ARBITRO /", () => {
    let token = "";
    beforeAll((done) => {
      req("http://localhost:3005")
        .post("/api/sesion/iniciar")
        .send({
          email: "empleado1@gmail.com",
          password:"empleado1",
        })
        .end((err, response) => {
          console.log(response.body);
          token = response.body.token; // save the token!
          done();
        });
    });
    test("Este test verificara la obtencion de un arbitro", async () => {
        const result = await req("http://localhost:3003")
        .get("/empleado/arbitro/")
        .set("Authorization", `Bearer ${token}`)
        .send();
        expect(result.statusCode).toBe(200);
    });
});

describe("PRUEBA UNITARIA ACTUALIZAR ARBITRO POR ID /", () => {
    let token = "";
    beforeAll((done) => {
      req("http://localhost:3005")
        .post("/api/sesion/iniciar")
        .send({
          email: "empleado1@gmail.com",
          password:"empleado1",
        })
        .end((err, response) => {
          //console.log(response.body);
          token = response.body.accessToken; // save the token!
          done();
        });
    });
    test("Este test verificara la actualizacion de un arbitro", async () => {
        const result = await req("http://localhost:3003")
        .put("/empleado/arbitro/")
        .set("Authorization", `Bearer ${token}`)
        .send({
            "id_arbitro": 1,
            "nombre": "Juan",
        })
    })
});


///////