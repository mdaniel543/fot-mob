require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const bodyParser = require("body-parser");

const app = express();

const jwt = require("jsonwebtoken");

const arbitro = require("./routes/arbitro.routes");
const equipo = require("./routes/equipo.routes");
const noticia = require("./routes/noticia.routes");
const empleado = require("./routes/empleado.routes");
const tecnico = require("./routes/tecnico.routes");
const partido = require("./routes/partido.routes");

app.set("port", process.env.PORT || 3003);
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(authenticateToken);

app.use("/empleado/arbitro/", arbitro);
app.use("/empleado/equipo", equipo);
app.use("/empleado/noticias/", noticia);
app.use("/empleado", empleado);
app.use("/empleado/tecnico/", tecnico);
app.use("/empleado/partido", partido);

app.get("/", (req, res) => {
  res.send("Hello! this is Admin Server");
});

app.listen(app.get("port"), () => {
  console.log("Server on port", app.get("port"));
});

function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(400);
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(400);
    //if (user.id_rol != 2) return res.sendStatus(400);
    req.user = user;
    next();
  });
}

// Comentario
