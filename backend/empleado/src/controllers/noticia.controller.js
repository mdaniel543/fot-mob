require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");



/**
 * @param {*} descripcion VARCHAR(200),
 * @param {*} id_equipo INT,
 * @param {*} id_usuario INT
 * obtener_noticias
 */
exports.post = async (req, res) => {
    const { descripcion, id_equipo, id_usuario } = req.body;
    if (!descripcion || !id_equipo || !id_usuario) {
        return res.status(400).json({ message: "Faltan datos" });
    }
    try {
        const sql = `CALL crear_noticia(?,?,?)`;
        const params = [descripcion, id_equipo, id_usuario];
        const result = await db .query(sql, params);
        res.status(200).json({ message: result[0][0].message });
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al crear noticia" });
    }
}

exports.get = async (req, res) => { 
    try {
        const sql = `CALL obtener_noticias()`;
        const result = await db.query(sql);
        res.status(200).json(result[0]);
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al obtener noticias" });
    }
}

