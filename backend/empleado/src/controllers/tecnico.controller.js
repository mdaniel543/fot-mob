const db = require('../db/conexion')

/**
 * obtener_tecnicos
 */

exports.get = async (req, res) => {
  try {
    const sql = `CALL obtener_tecnicos()`
    const result = await db.query(sql)
    res.status(200).json(result[0])
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al obtener tecnicos' })
  }
}

/**
 * @body {*} nombre : string,
 * @body {*} fecha_nac : string,
 */
exports.post = async (req, res) => {
  const { nombre, fecha_nac } = req.body
  if (!nombre || !fecha_nac) {
    return res.status(400).json({ message: 'Faltan datos' })
  }
  try {
    const sql = `CALL crear_tecnico(?,?)`
    const params = [nombre, fecha_nac]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al crear tecnico' })
  }
}

/**
 * @param {*} id : int
 * @body {*} nombre : string,
 */
exports.put = async (req, res) => {
  const { id } = req.params
  const { nombre } = req.body
  if (!nombre) {
    return res.status(400).json({ message: 'Faltan datos' })
  }
  try {
    const sql = `CALL actualizar_tecnico(?,?)`
    const params = [id, nombre]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al actualizar tecnico' })
  }
}

exports.delete = async (req, res) => {
  const { id } = req.params
  if (!id) {
    return res.status(400).json({ message: 'Faltan datos' })
  }
  try {
    const sql = `CALL eliminar_tecnico(?)`
    const params = [id]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al eliminar tecnico' })
  }
}
