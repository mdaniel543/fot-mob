require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

exports.obtenerPartido = async (req, res) => {
  try {
    const sql = `CALL obtener_partidos()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener partidos" });
  }
};

/**
 * @param {*} pubico INT,
 * @param {*} hora VARCHAR(100),
 * @param {*} fecha VARCHAR(100),
 * @param {*} equipo_loc INT
 * @param {*} equipo_vis INT
 * @param {*} competencia INT
 * @param {*} estadio INT
 * @param {*} arbitro INT
 */
exports.crearPartido = async (req, res) => {
  const {
    publico,
    hora,
    fecha,
    equipo_loc,
    equipo_vis,
    competencia,
    estadio,
    arbitro,
  } = req.body;

  if (
    !publico ||
    !hora ||
    !fecha ||
    !equipo_loc ||
    !equipo_vis ||
    !competencia ||
    !estadio ||
    !arbitro
  ) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    // const sql = `CALL crear_partido(${publico},"",'${fecha}',${equipo_loc},${equipo_vis},${competencia},$})`;
    const sql = `CALL crear_partido(?,?,?,?,?,?,?,?)`;
    const params = [
      publico,
      hora,
      fecha,
      equipo_loc,
      equipo_vis,
      competencia,
      estadio,
      arbitro,
    ];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al crear partido" });
  }
};

/**
 * @param {*} id_partido INT,
 * @param {*} hora VARCHAR(20),
 * @param {*} fecha VARCHAR(20)
 */
exports.actualizarPartido = async (req, res) => {
  const { id_partido } = req.params;
  const { hora, fecha } = req.body;

  if (!id_partido || !hora || !fecha) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL actualizar_partido(?,?,?)`;
    const params = [id_partido, hora, fecha];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al actualizar partido" });
  }
};

/**
 * @param {*} id_equipo INT
 */
exports.eliminarPartido = async (req, res) => {
  const { id_partido } = req.params;

  if (!id_partido) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL eliminar_partido(?)`;
    const params = [id_partido];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al eliminar equipo" });
  }
};
