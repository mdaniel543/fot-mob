require('dotenv/config')
const bcrypt = require('bcrypt')
const db = require('../db/conexion')
const jwt = require('jsonwebtoken')
const moment = require('moment')

exports.obtenerEquipo = async (req, res) => {
  try {
    const sql = `CALL obtener_equipos()`
    const result = await db.query(sql)
    res.status(200).json(result[0])
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al obtener equipos' })
  }
}

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} id_pais INT,
 * @param {*} fecha_fun date,
 * @param {*} tipo INT
 */
exports.crearEquipo = async (req, res) => {
  const { nombre, id_pais, fecha, tipo } = req.body

  if (!nombre || !id_pais || !fecha || !tipo) {
    return res.status(400).json({ message: 'Faltan datos' })
  }

  //Castear fecha
  // let fecha = moment(fecha_fun).format("DD-MM-YYYY");

  try {
    const sql = `CALL crear_equipo(?,?,?,?)`
    const params = [nombre, id_pais, fecha, tipo]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al crear equipo' })
  }
}

/**
 * @param {*} id_equipo INT,
 * @param {*} nombre VARCHAR(100)
 */
exports.actualizarEquipo = async (req, res) => {
  const { id_equipo, nombre } = req.body

  if (!id_equipo || !nombre) {
    return res.status(400).json({ message: 'Faltan datos' })
  }

  try {
    const sql = `CALL actualizar_equipo(?,?)`
    const params = [id_equipo, nombre]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al actualizar equipo' })
  }
}

/**
 * @param {*} id_equipo INT
 */
exports.eliminarEquipo = async (req, res) => {
  const { id_equipo } = req.params
  if (!id_equipo) {
    return res.status(400).json({ message: 'Faltan datos' })
  }

  try {
    const sql = `CALL eliminar_equipo(?)`
    const params = [id_equipo]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al eliminar equipo' })
  }
}
