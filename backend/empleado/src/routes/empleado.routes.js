const express = require("express");
const router = express.Router();

const empleado = require("../controllers/empleado.controller");

router.get("/bitacora/:rol/:id_deportista", empleado.bitacora);

router.put("/transferir/:rol", empleado.transferir);

router.get("/jugadores_equipo/:id_equipo", empleado.jugador_equipo);

router.put("/incidencia/estado", empleado.estadoPartido);

router.post("/incidencia/gol",empleado.agregar_gol);

router.post("/incidencia/tarjeta",empleado.agregar_tarjeta);

router.post("/competencia/equipo",empleado.agregar_equipo_competencia);

router.put("/competencia/equipo",empleado.actualizar_equipo_competencia);

router.get("/competencia/equipos/:opcion/:id_competencia",empleado.obtener_equipos_competencia);

module.exports = router;