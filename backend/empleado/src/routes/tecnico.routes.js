const express = require("express");
const router = express.Router();

const tecnico = require("../controllers/tecnico.controller");

router.get("/", tecnico.get);

router.post("/", tecnico.post);

router.put("/:id", tecnico.put);

router.delete("/:id", tecnico.delete);


module.exports = router;