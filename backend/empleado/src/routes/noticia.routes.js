const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const noticia = require("../controllers/noticia.controller");

router.get("/", noticia.get);

router.post("/", noticia.post);

module.exports = router;
