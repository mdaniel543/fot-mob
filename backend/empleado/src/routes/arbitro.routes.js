const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const arbitro = require("../controllers/arbitro.controller");

router.get("/", arbitro.obtenerArbitro);
router.post("/", arbitro.crearArbitro);
router.put("/", arbitro.actualizarArbitro);
router.delete("/", arbitro.eliminarArbitro);

module.exports = router;
